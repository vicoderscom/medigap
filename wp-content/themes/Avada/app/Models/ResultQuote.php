<?php

namespace App\Models;

use NF\Database\Eloquent\Model;

/**
 *
 */
class ResultQuote extends Model
{
    /**
     * [$table_name name of table]
     * @var string
     */
    protected $table = PREFIX_TABLE . 'result_quote';

    /**
     * [$primary_id primary key of table]
     * @var string
     */
    protected $primary_key = 'id';

    const STATUS_TRUE  = 1;
    const STATUS_FALSE = 0;

    protected $fillable = ['zip', 'plan', 'age', 'gender', 'tobacco', 'result', 'created_at', 'updated_at'];

}
