<?php

use App\Models\ResultQuote;
use App\Models\UserQuote;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Collection;
/**
 * Template Name: Result
 *
 * @package Avada
 * @subpackage Templates
 */

get_header();

if ( 
    ! isset( $_GET['vc_nonce'] ) 
    || ! wp_verify_nonce( $_GET['vc_nonce'], 'vc_quote' )
    || ! isset( $_GET['firstname'] )
    || ! isset( $_GET['lastname'] )
    || ! isset( $_GET['zipcode'] )
    || ! isset( $_GET['phone'] )
    || ! isset( $_GET['plantype'] )
    || ! isset( $_GET['age'] )
    || ! isset( $_GET['gender'] )
    || ! isset( $_GET['tobacco'] )
) {
    wp_redirect(esc_url( home_url( '/' ) ));
    exit;
}
?>

<!-- Event snippet for Medigap Quote Submission conversion page -->
<script>
gtag('event', 'conversion', {'send_to': 'AW-1067751615/Qg-lCPa-23gQv7GS_QM'});
</script>

<?php

$save_result = '';
// if (empty($_GET)) {
//     wp_redirect(home_url());
//     exit;
// }
$quote = ResultQuote::where('plan', $_GET['plantype'])
    ->where('gender', $_GET['gender'])
    ->where('age', $_GET['age'])
    ->where('tobacco', $_GET['tobacco'])
    ->where('zip', $_GET['zipcode'])
    ->first();
if (!$quote) {
    $request_api = 'https://eisapp.com/api/rates/';
    $params      = [
        'key'     => '7a5163516b8bdb758aa0e92f3f3bfb2c',
        'product' => 'ms',
        'plan'    => $_GET['plantype'],
        'gender'  => $_GET['gender'],
        'age'     => $_GET['age'],
        'tobacco' => $_GET['tobacco'],
        'zip'     => $_GET['zipcode'],
    ];

    $client = new Client();
    try {
        $res         = $client->request('GET', $request_api, [RequestOptions::QUERY => $params]);
        $body        = $res->getBody();
        $status_code = $res->getStatusCode();
    } catch (RequestException $e) {
        echo Psr7\str($e->getRequest());
        if ($e->hasResponse()) {
            echo Psr7\str($e->getResponse());
        }
        die;
    }
    if ($status_code !== 200) {
        $res = [
            'code'    => '406',
            'message' => 'No content',
        ];
        echo json_encode($res);
        die;
    }

    $result_plan = json_decode($body, true);
    $result_plan = new Collection($result_plan['result']);
    $result_plan = $result_plan->map(function($item, $key){
        // $item['hide_name'] = false;
        // if($key <= 2 ) {
            $item['hide_name'] = true;
        // }
        return $item;
    });

    $save_result_to_db = json_encode($result_plan);

    $quote          = new ResultQuote;
    $quote->zip     = $_GET['zipcode'];
    $quote->plan    = $_GET['plantype'];
    $quote->age     = $_GET['age'];
    $quote->gender  = $_GET['gender'];
    $quote->tobacco = $_GET['tobacco'];
    $quote->result  = $save_result_to_db;
    $response       = $quote->save();
}

// $user_quote            = new UserQuote();
// $user_quote->firstname = $_GET['firstname'];
// $user_quote->lastname  = $_GET['lastname'];
// $user_quote->phone     = $_GET['phone'];
// $user_quote->email     = $_GET['email'];
// $user_quote->result_id = $quote->id;
// $user_quote->save();

$user_quote_id = uniqid();

$user_data = [
    'uid'       => $user_quote_id,
    'firstname' => ucwords(strtolower($_GET['firstname'])),
    'lastname'  => ucwords(strtolower($_GET['lastname'])),
    'phone'     => $_GET['phone'],
    'email'     => $_GET['email'],
    'age'       => $_GET['age'],
    "gender"    => $_GET['gender'],
    "plan"      => $_GET['plantype'],
    "tobacco"   => $_GET['tobacco'],
    "zipcode"   => $_GET['zipcode'],
    "date"      => getDateDataUser(),
    "time"      => getTimeDataUser(),
    "client_ip" => get_client_ip(),
    "platform"  => getFlatform(),
];
$result_plan_decode = json_decode($quote->result, true);

$result_collection = new Collection($result_plan_decode);

if(!empty($_GET)){
    foreach ($_GET as $key => $value) {
        switch ($value) {
            case 'asc':
                $result_collection = $result_collection->sortBy($key);
                break;
            case 'desc':
                $result_collection = $result_collection->sortByDesc($key);
                break;
            default:
                break;
        }
    }
}

$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

$data = [
    'info'        => $user_data,
    'result_plan' => $result_collection,
    'actual_link' => $actual_link
];

view('templates.template-result', $data);

if(
   $_GET['vc_token_key'] === $_SESSION['vc_token_key']
) {
    $user_data['source'] = $_GET['source'];
    $emails[] = get_option('admin_email');
    if ( defined('VC_EMAILS') && ! empty(json_decode(VC_EMAILS, true)) ) {
        $emails = json_decode(VC_EMAILS, true);
    }

    // Send mail.
    foreach ($emails as $item) {
        sendMailToSalePerson($item, 'MG-MC New Lead - ' . (strtoupper($_GET['lastname'])) . ', ' . $_GET['firstname'], $user_data, '');
    }

    require_once Avada::$template_dir_path . '/functions-pipe.php';

    unset($_SESSION['vc_token_key']);
}
?>

<?php do_action('avada_after_content');?>
<?php get_footer();?>
