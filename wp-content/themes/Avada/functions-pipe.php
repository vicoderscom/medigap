<?php

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

$api_token = 'b02f06e8ab610bb76b42a2a425b44ec57cdff850';

// Deal title and Organization ID
$deal = [
    '02aea7be6642aacd7dc06967b74b6b899ce2652e' => $user_quote_id,
    'b102a6d7a21021efb16f7ba10c3b229de03da1fb' => $_GET['plantype'],
    'd48d2fade8d058307d8fdffcacc6975b9851b2fe' => $_GET['gender'],
    '828c0cc2b4cfe8feb12244196b133ccd76136c24' => $_GET['age'],
    '05a2cc2553f816761da51354f14828121aa3ce64' => $_GET['tobacco'],
    '7654b9941368ad1aa841368b8358690f1c0ac764' => $_GET['zipcode'],
    'ff100af275ae27141f320c13fd6bba5cf4349f00' => ucwords(strtolower($_GET['firstname'])),
    '32bb597834f4fb5809acbdc99520dccb92b9237c' => ucwords(strtolower($_GET['lastname'])),
    "name"                                     => ucwords(strtolower($_GET['firstname'] . ' ' . $_GET['lastname'])),
    "owner_id"                                 => "3253989",
    "email"                                    => $_GET['email'],
    "phone"                                    => $_GET['phone'],
    "add_time"                                 => getDateDataUser() . ' ' . getTimeDataUser(),
    "visible_to"                               => "3",
    "1bac1769554eb660847c29d667af912912abac73" => getFlatform(),
    "c16c4315b2c059a2fccc112ee0c11612992ee81e" => get_client_ip(),
    "131c647b2c72c18d7c549309411c6450a8f9ba51" => $_GET['source']
];

$url = 'https://api.pipedrive.com/v1/persons?api_token=' . $api_token;

$client = new Client();
try {
    $res  = $client->request('POST', $url, ['form_params' => $deal]);
    $body = $res->getBody();
} catch (RequestException $e) {
    echo Psr7\str($e->getRequest());
    if ($e->hasResponse()) {
        echo Psr7\str($e->getResponse());
    }
    die;
}
$result = json_decode($body, true);

// Create deal
if (false === $result['success']) {
    $user_res = [
        'code'    => '406',
        'message' => 'No content',
    ];
    echo json_encode($user_res);
    die;
}

$result = $result['data'];

$deal = [
    "title"      => $result['name'] . ' deal',
    "user_id"    => "3253989",
    "person_id"  => $result['id'],
    "stage_id"   => "1", 
    "status"     => "open",
    "visible_to" => "3",
];
$deal_api = 'https://api.pipedrive.com/v1/deals?api_token=' . $api_token;

try {
    $deal_res = $client->request('POST', $deal_api, ['form_params' => $deal]);
} catch (RequestException $e) {
    echo Psr7\str($e->getRequest());
    if ($e->hasResponse()) {
        echo Psr7\str($e->getResponse());
    }
    die;
}
