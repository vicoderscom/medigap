<?php
$status = false;
$message = "Some error occured. Plz try again";
$result_data = array();
if( isset($_POST['email']) && isset($_POST["fname"]) && isset($_POST["lname"]) && isset($_POST["phone"]) && isset($_POST["zip_code"]) && isset($_POST["age"]) && isset($_POST["gender"]) && isset($_POST["tobacco"]) && isset($_POST["plantype"]) ){

    $key = "7a5163516b8bdb758aa0e92f3f3bfb2c";
    $product = "ms";
    $plan = $_POST["plantype"];
    $gender = $_POST["gender"];
    $age = $_POST["age"];
    $tobacco = $_POST["tobacco"];
    $zip = $_POST["zip_code"];
     /* $plan = "A";
    $gender = "Male";
    $age = 70;
    $tobacco = "Tobacco";
    $zip = "45623";*/
    $url = "https://eisapp.com/api/rates/?key=".$key."&product=".$product."&plan=".$plan."&gender=".$gender."&age=".$age."&tobacco=".$tobacco."&zip=".$zip;

    $curl = curl_init();
    curl_setopt ($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec ($curl);
    curl_close ($curl);
    if ( !empty($result)  ) {

        $result = json_decode($result,true);
        if (isset($result["status"]) && $result["status"] == "ok" ) {

            $result_data = $result["result"];
            if( !empty($result_data) ) {
                $message = "Here are your quote results";

            } else {

                $message = "Sorry no quote results found for the provided info";

            }
            $status = true;
            
        } else {

           $message = $result["message"];

        }

    } 
} else {

    $message = "Please fill all the fields";
}

echo json_encode(array("status" => $status,"message" => $message, "data" => $result_data));