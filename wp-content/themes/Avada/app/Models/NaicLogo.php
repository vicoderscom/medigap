<?php

namespace App\Models;

use NF\Database\Eloquent\Model;

/**
 *
 */
class NaicLogo extends Model
{
    /**
     * [$table_name name of table]
     * @var string
     */
    protected $table = PREFIX_TABLE . 'naic_logo';

    /**
     * [$primary_id primary key of table]
     * @var string
     */
    protected $primary_key = 'id';

    protected $fillable = ['naic', 'link_logo', 'created_at', 'updated_at'];

}
