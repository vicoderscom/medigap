<?php 
use App\Models\NaicLogo;
?>
<section class="page-result">
	<div class="container result-container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-push-1 page-result-content">
				<div class="result-meta">
					<?php 
					echo '<div>'.$info['firstname'].', based upon this information, please find your best options below:</div>
					<br>
					<div>
						<span>Age </span>
						<span>'.$info['age'].'</span>
					</div>
					<div>
						<span>Zip code </span>
						<span>'.$info['zipcode'].'</span>
					</div>
					<div class="'.$info['gender'].'">
						<span>Gender </span>
						<div>
							<span class="Male">Male</span>
							<span class="Female">Female</span>
						</div>
					</div>
					<div class="'.$info['tobacco'].'">
						<span>Tobacco Use </span>
						<div>
							<span class="Tobacco">Yes</span>
							<span class="Non-Tobacco">No</span>
						</div>
					</div>
					<br>
					<div>Call <a href="tel:8668534791">(866) 853-4791</a> to Enroll Now or if you have any questions.</div>
					<div>Our friendly and knowledgeable expert agents are here to assist you.</div>
					';
					?>
				</div>
				<div id="sort_container" class="pull-right hidden">
					<table>
						<tr>
							<td>
								<p><label>Company sort</label></p>
								<select id="company_sort" name="company">
									<option value="">-- choose a option --</option>
									<option value="asc" <?php echo ($_GET['company'] == 'asc' ? 'selected' : ''); ?>>From A - Z</option>	
									<option value="desc" <?php echo ($_GET['company'] == 'desc' ? 'selected' : ''); ?>>From Z - A</option>	
								</select>
							</td>
							<td>
								<p><label>Rating sort</label></p>
								<select id="rate_sort" name="rate">
									<option value="">-- choose a option --</option>
									<option value="asc" <?php echo ($_GET['am_best'] == 'asc' ? 'selected' : ''); ?>>From A - Z</option>	
									<option value="desc" <?php echo ($_GET['am_best'] == 'desc' ? 'selected' : ''); ?>>From Z - A</option>	
								</select>
							</td>
							<td>
								<p><label>Price sort</label></p>
								<select id="price_sort" name="price">
									<option value="">-- choose a option --</option>
									<option value="asc" <?php echo ($_GET['monthly'] == 'asc' ? 'selected' : ''); ?>>From low to high</option>	
									<option value="desc" <?php echo ($_GET['monthly'] == 'desc' ? 'selected' : ''); ?>>From high to low</option>	
								</select>
							</td>
							<td>
								<input type="hidden" class="actual_link" name="actual_link" value="<?php echo $actual_link; ?>">
								<button type="button" class="btn btn-default sort_btn">Sort result</button>
							</td>
						</tr>
					</table>
				</div>
				<div class="clearfix"></div>
					<?php 
					$i = 1;
					if(!empty($result_plan)):
						foreach ($result_plan as $key => $item): 	
							$naic_logo = NaicLogo::where('naic', $item['naic'])->first();
							$naic_logo_src = $naic_logo['link_logo'];
							if(empty($naic_logo_src)) {
								$naic_logo_src = get_template_directory_uri().'/assets/images/logo-result.png';
							}
					?>
					<div class="result-item">
						<div class="result-item-title">
							<h4>
								<?php
									// if($item['hide_name']) {
										// echo 'Please Call to Reveal Carrier: <a href="tel:8668534791">(866) 853-4791</a>';
									// } else {
									// 	echo $item['company'] . ' - ' . $item['description']; 
									// }
								?>
								<?php
									if($i > 3) {
										echo 'Insurance Carrier: <span>'.$item['company'].'</span>';
									} else {
										echo 'Please Call to Reveal Carrier: <a href="tel:8668534791">(866) 853-4791</a>';
									}
								?>
							</h4>
						</div>
						<div class="result-item-content">
							<div class="stt">
								<span>No.</span>
								<?php echo $i++; ?>
							</div>
							<div class="images_result">
								<span>Insurance Co.</span>
								<?php
									// if($item['hide_name']) {
									// 	echo '<img src="'.get_template_directory_uri().'/assets/images/logo-result.png" />';
									// } else {
									// 	echo '<img src="'.$naic_logo_src.'" alt="'.$item['company'].'">';
									// }
									// $i++;
								?>

								<?php
									if($i > 4) {
										echo '<p><img src="'.$naic_logo_src.'" /></p>';
									} else {
										echo '<p class="f-25">Contact</p>';
										echo '<p><img src="'.get_template_directory_uri().'/assets/images/logo-result.png" /></p>';
										echo '<p class="f-22">For Details</p>';
									}
								?>								
								
							</div>
							<div class="am_best">
								<span>Carrier Rating</span>
								<?php echo $item['am_best']; ?>
							</div>
							<div class="plan">
								<span>Plan Type</span>
								Plan <?php echo $item['plan']; ?>
							</div>
							<div class="monthly">
								<span>Amount</span>
								$<?php echo $item['monthly']; ?> Monthly
							</div>
							<div class="call_now">
								<p>Call Now</p>
								<div class="call">
									<a href="tel:8668534791">(866) 853-4791</a>
								</div>
							</div>
							<div class="error-info">
								<span></span>
								<div class="enroll">
									<a class="btn btn-info btn-lg vc-enroll" data-toggle="modal" data-target="#modal-enroll_<?php echo $i; ?>">
										Enroll
<div style="visibility: hidden;">
	<input type="hidden" class="vc-firstname" data-firstname="<?php echo $info['firstname']; ?>">
	<input type="hidden" class="vc-lastname" data-lastname="<?php echo $info['lastname']; ?>">
	<input type="hidden" class="vc-gender" data-gender="<?php echo $item['gender']; ?>">
	<input type="hidden" class="vc-zipcode" data-zipcode="<?php echo $item['zipcode']; ?>">
	<input type="hidden" class="vc-age" data-age="<?php echo $item['age']; ?>">
	<input type="hidden" class="vc-tobacco" data-tobacco="<?php echo $item['tobacco']; ?>">
	<input type="hidden" class="vc-company" data-company="<?php echo $item['company']; ?>">
	<input type="hidden" class="vc-ambest" data-ambest="<?php echo $item['am_best']; ?>">
	<input type="hidden" class="vc-plan" data-plan="<?php echo $item['plan']; ?>">
	<input type="hidden" class="vc-monthly" data-monthly="<?php echo $item['monthly']; ?>">
	<input type="hidden" class="vc-freeconsult" data-freeconsult="<?php echo $info['freeconsult']; ?>">
</div>
									</a>
									<div class="modal fade" id="modal-enroll_<?php echo $i; ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<p class="modal-title">Thanks <?php echo $info['firstname']; ?></p>
												</div>
												<div class="modal-body">
													<p>Thank you for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="more-info">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-more-info_<?php echo $i; ?>">More-info</a>
									<div class="modal fade" id="modal-more-info_<?php echo $i; ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<p class="modal-title">Thanks <?php echo $info['firstname']; ?></p>
												</div>
												<div class="modal-body">
													<p>Thank you for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endforeach;
					else:
						?>
						<div class="result-item">
							<div class="result-item-title">
								<h4>Result search</h4>
							</div>
							<span class="text-center"><?php echo 'No data for plan ' . $info['plan'] . '. Let see other part!'; ?></span>
						</div>
						<?php
					endif;
					//==================== for other part ===================
					$j = 1;
					if(!empty($other_plan)):
						echo '<hr class="hr-class">';
						echo '<h3 class="text-center">Other Plan for you</h3>';
						foreach ($other_plan as $key => $item): 
							if($j > 30 ){
								break;
							}
							if(empty($item['company'])) {
								continue;
							}
							$naic_logo = NaicLogo::where('naic', $item['naic'])->first();
							$naic_logo_src = $naic_logo['link_logo'];
							if(empty($naic_logo_src)) {
								$naic_logo_src = get_template_directory_uri().'/assets/images/logo-result.png';
							}							
					?>
					<div class="result-item other-plan hidden">
						<div class="result-item-title">
							<h4>
								<?php
									// if($item['hide_name']) {
										// echo 'Please Call to Reveal Carrier: <a href="tel:8668534791">(866) 853-4791</a>';
									// } else {
									// 	echo $item['company'] . ' - ' . $item['description']; 
									// }
								?>
								<?php
									if($j > 3) {
										echo 'Insurance Carrier: <span>'.$item['company'].'</span>';
									} else {
										echo 'Please Call to Reveal Carrier: <a href="tel:8668534791">(866) 853-4791</a>';
									}
								?>
							</h4>
						</div>
						<div class="result-item-content">
							<div><?php echo $j++; ?></div>
							<div>
								<?php
									if($j > 4) {
										echo '<p><img src="'.$naic_logo_src.'" /></p>';
									} else {
										echo '<p class="f-25">Contact</p>';
										echo '<p><img src="'.get_template_directory_uri().'/assets/images/logo-result.png" /></p>';
										echo '<p class="f-22">For Details</p>';
									}
								?>
							</div>
							<div><?php echo $item['am_best']; ?></div>
							<div>Plan <?php echo $item['plan']; ?></div>
							<div>$<?php echo $item['monthly']; ?> Monthly</div>
							<div>
								<div class="enroll">
									<a class="btn btn-info btn-lg vc-enroll" data-toggle="modal" data-target="#modal-enroll_<?php echo $i; ?>">
										Enroll
<div style="visibility: hidden;">
	<input type="hidden" class="vc-firstname" data-firstname="<?php echo $info['firstname']; ?>">
	<input type="hidden" class="vc-lastname" data-lastname="<?php echo $info['lastname']; ?>">
	<input type="hidden" class="vc-gender" data-gender="<?php echo $item['gender']; ?>">
	<input type="hidden" class="vc-zipcode" data-zipcode="<?php echo $item['zipcode']; ?>">
	<input type="hidden" class="vc-age" data-age="<?php echo $item['age']; ?>">
	<input type="hidden" class="vc-tobacco" data-tobacco="<?php echo $item['tobacco']; ?>">
	<input type="hidden" class="vc-company" data-company="<?php echo $item['company']; ?>">
	<input type="hidden" class="vc-ambest" data-ambest="<?php echo $item['am_best']; ?>">
	<input type="hidden" class="vc-plan" data-plan="<?php echo $item['plan']; ?>">
	<input type="hidden" class="vc-monthly" data-monthly="<?php echo $item['monthly']; ?>">
	<input type="hidden" class="vc-freeconsult" data-freeconsult="<?php echo $info['freeconsult']; ?>">
</div>
									</a>
									<div class="modal fade" id="modal-enroll-<?php echo $j; ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<p class="modal-title">Thanks <?php echo $info['firstname']; ?></p>
												</div>
												<div class="modal-body">
													<p>Thank your for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="more-info">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-more-info-<?php echo $j;  ?>">More-info</a>
									<div class="modal fade" id="modal-more-info-<?php echo $j;  ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<p class="modal-title">Thanks <?php echo $info['firstname']; ?></p>
												</div>
												<div class="modal-body">
													<p>Thank your for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endforeach;
					endif;

					if(empty($result_plan) && empty($other_plan)):
					?>
					<div class="result-item">
						<div class="result-item-title">
							<h4>American Retirement Life Insurance</h4>
						</div>
						<span class="text-center">No data</span>
					</div>
					<?php
					endif;
					?>
				{{-- <div class="result-button">
					<a class="click_other_part" href="javascript:void(0);" title="">Other parts</a>
				</div> --}}
			</div>
		</div>
	</div>
</section>