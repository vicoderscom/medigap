<?php

namespace App\RestApi;

/**
 *
 */
class QuoteApi extends \WP_REST_Controller
{
    /**
     * [$base The base to use in the API route]
     * @var string
     */
    protected $rest_base = 'quote-api';

    /**
     * [$namespace namespace for routes API]
     * @var string
     */
    protected $namespace = 'wp/v2';

    /**
     * [__construct description]
     */
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'register_routes']);
    }

    public function register_routes()
    {
        register_rest_route($this->namespace, "/{$this->rest_base}", [
            'methods'             => \WP_REST_Server::READABLE,
            'callback'            => [$this, 'quote_request'],
            'permission_callback' => [$this, 'get_items_permissions_check'],
        ]);
    }

    /**
     * [get_items get collection of items ]
     *
     * @param WP_REST_Request $request Full data about the request
     *
     * @return WP_Error|WP_REST_Response
     */
    public function quote_request($request)
    {
        $response = [
            'status'  => 'ok',
            'message' => 'Success',
            'result'  => [
                [
                    "company"     => "ARLIC",
                    "description" => "American Retirement Life Insurance Company",
                    "naic"        => "88366",
                    "am_best"     => "A-",
                    "plan"        => "G",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "68.03",
                    "quarterly"   => "341.39",
                    "semi"        => "668.01",
                    "annual"      => "1476.94",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "ARLIC",
                    "description" => "American Retirement Life Insurance Company",
                    "naic"        => "16691",
                    "am_best"     => "A-",
                    "plan"        => "G",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Male",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "125.03",
                    "quarterly"   => "291.39",
                    "semi"        => "568.01",
                    "annual"      => "1076.94",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "ARLIC",
                    "description" => "American Retirement Life Insurance Company",
                    "naic"        => "12628",
                    "am_best"     => "A-",
                    "plan"        => "G",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Male",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "113.03",
                    "quarterly"   => "381.39",
                    "semi"        => "768.01",
                    "annual"      => "1466.94",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "ARLIC",
                    "description" => "American Retirement Life Insurance Company",
                    "naic"        => "12321",
                    "am_best"     => "A-",
                    "plan"        => "F",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "130.03",
                    "quarterly"   => "402.39",
                    "semi"        => "798.01",
                    "annual"      => "1496.94",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "ARLIC",
                    "description" => "American Retirement Life Insurance Company",
                    "naic"        => "63274",
                    "am_best"     => "A-",
                    "plan"        => "L",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Male",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "123.03",
                    "quarterly"   => "391.39",
                    "semi"        => "768.01",
                    "annual"      => "1476.94",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "ARLIC",
                    "description" => "American Retirement Life Insurance Company",
                    "naic"        => "16691",
                    "am_best"     => "A-",
                    "plan"        => "M",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "123.03",
                    "quarterly"   => "391.39",
                    "semi"        => "768.01",
                    "annual"      => "1476.94",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "ARLIC",
                    "description" => "American Retirement Life Insurance Company",
                    "naic"        => "71390",
                    "am_best"     => "A-",
                    "plan"        => "M",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "123.03",
                    "quarterly"   => "391.39",
                    "semi"        => "768.01",
                    "annual"      => "1476.94",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "ARLIC",
                    "description" => "American Retirement Life Insurance Company",
                    "naic"        => "69752",
                    "am_best"     => "A-",
                    "plan"        => "M",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "123.03",
                    "quarterly"   => "391.39",
                    "semi"        => "768.01",
                    "annual"      => "1476.94",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "ARLIC",
                    "description" => "American Retirement Life Insurance Company",
                    "naic"        => "42129",
                    "am_best"     => "A-",
                    "plan"        => "M",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "123.03",
                    "quarterly"   => "391.39",
                    "semi"        => "768.01",
                    "annual"      => "1476.94",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "Bankers Fidelity",
                    "description" => "Bankers Fidelity Life Insurance Company (Standard)",
                    "naic"        => "61239",
                    "am_best"     => "A-",
                    "plan"        => "G",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "144.00",
                    "quarterly"   => "432.00",
                    "semi"        => "864.00",
                    "annual"      => "1728.00",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "Manhattan Life",
                    "description" => "Manhattan Life Insurance Company",
                    "naic"        => "65870",
                    "am_best"     => "B+",
                    "plan"        => "G",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "143.92",
                    "quarterly"   => "431.75",
                    "semi"        => "863.50",
                    "annual"      => "1727.00",
                    "fee"         => "",
                    "hhd"         => "7%",
                ],
                [
                    "company"     => "Qualchoice",
                    "description" => "Qualchoice Life",
                    "naic"        => "70998",
                    "am_best"     => "Not Rated",
                    "plan"        => "G",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "141.65",
                    "quarterly"   => "424.95",
                    "semi"        => "849.90",
                    "annual"      => "1699.80",
                    "fee"         => "",
                    "hhd"         => "",
                ],
                [
                    "company"     => "Blue Cross Blue Shield",
                    "description" => "Arkansas Blue Cross Blue Shield",
                    "naic"        => "83470",
                    "am_best"     => "A",
                    "plan"        => "G",
                    "state"       => "AR",
                    "area"        => "2",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "133.90",
                    "quarterly"   => "401.70",
                    "semi"        => "803.40",
                    "annual"      => "1606.80",
                    "fee"         => "",
                    "hhd"         => "",
                ],
                [
                    "company"     => "CSI",
                    "description" => "Central States Indemnity Company of Omaha",
                    "naic"        => "34274",
                    "am_best"     => "A+",
                    "plan"        => "G",
                    "state"       => "AR",
                    "area"        => "1",
                    "gender"      => "Female",
                    "tobacco"     => "Tobacco",
                    "rate_type"   => "Community Rated",
                    "age"         => "68",
                    "monthly"     => "129.58",
                    "quarterly"   => "388.75",
                    "semi"        => "777.50",
                    "annual"      => "1555.00",
                    "fee"         => "",
                    "hhd"         => "",
                ],
            ],
        ];

        $response = rest_ensure_response(($response));
        return $response;
    }

    /**
     * Check if a given request has access to get items.
     *
     * @param WP_REST_Request $request Full data about the request.
     *
     * @return WP_Error|bool
     */
    public function get_items_permissions_check($request)
    {
        // return current_user_can('edit_posts');
        return true;
    }

    /**
     * Prepare the item for the REST response.
     *
     * @param stdClass        $item    WordPress representation of the item.
     * @param WP_REST_Request $request Request object.
     *
     * @return mixed
     */
    public function prepare_item_for_response($item, $request)
    {

    }
}
