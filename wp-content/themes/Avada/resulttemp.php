<?php /*Template Name: Custom Result Page */ ?>

<?php get_header(); ?>
<section id="content 123" <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php echo fusion_render_rich_snippets_for_pages(); // WPCS: XSS ok. ?>
			<?php if ( ! post_password_required( $post->ID ) ) : ?>
				<?php if ( Avada()->settings->get( 'featured_images_pages' ) ) : ?>
					<?php if ( 0 < avada_number_of_featured_images() || get_post_meta( $post->ID, 'pyre_video', true ) ) : ?>
						<div class="fusion-flexslider flexslider post-slideshow">
							<ul class="slides">
								<?php if ( get_post_meta( $post->ID, 'pyre_video', true ) ) : ?>
									<li>
										<div class="full-video">
											<?php echo get_post_meta( $post->ID, 'pyre_video', true ); // WPCS: XSS ok. ?>
										</div>
									</li>
								<?php endif; ?>
								<?php if ( has_post_thumbnail() && 'yes' != get_post_meta( $post->ID, 'pyre_show_first_featured_image', true ) ) : ?>
									<?php $attachment_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
									<?php $full_image       = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
									<?php $attachment_data  = wp_get_attachment_metadata( get_post_thumbnail_id() ); ?>
									<li>
										<a href="<?php echo esc_url_raw( $full_image[0] ); ?>" data-rel="iLightbox[gallery<?php the_ID(); ?>]" title="<?php echo esc_attr( get_post_field( 'post_excerpt', get_post_thumbnail_id() ) ); ?>" data-title="<?php echo esc_attr( get_post_field( 'post_title', get_post_thumbnail_id() ) ); ?>" data-caption="<?php echo esc_attr( get_post_field( 'post_excerpt', get_post_thumbnail_id() ) ); ?>">
											<img src="<?php echo esc_url_raw( $attachment_image[0] ); ?>" alt="<?php echo esc_attr( get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ) ); ?>" role="presentation" />
										</a>
									</li>
								<?php endif; ?>
								<?php $i = 2; ?>
								<?php while ( $i <= Avada()->settings->get( 'posts_slideshow_number' ) ) : ?>
									<?php $attachment_new_id = fusion_get_featured_image_id( 'featured-image-' . $i, 'page' ); ?>
									<?php if ( $attachment_new_id ) : ?>
										<?php $attachment_image = wp_get_attachment_image_src( $attachment_new_id, 'full' ); ?>
										<?php $full_image       = wp_get_attachment_image_src( $attachment_new_id, 'full' ); ?>
										<?php $attachment_data  = wp_get_attachment_metadata( $attachment_new_id ); ?>
										<li>
											<a href="<?php echo esc_url_raw( $full_image[0] ); ?>" data-rel="iLightbox[gallery<?php the_ID(); ?>]" title="<?php echo esc_attr( get_post_field( 'post_excerpt', $attachment_new_id ) ); ?>" data-title="<?php echo esc_attr( get_post_field( 'post_title', $attachment_new_id ) ); ?>" data-caption="<?php echo esc_attr( get_post_field( 'post_excerpt', $attachment_new_id ) ); ?>">
												<img src="<?php echo esc_url_raw( $attachment_image[0] ); ?>" alt="<?php echo esc_attr( get_post_meta( $attachment_new_id, '_wp_attachment_image_alt', true ) ); ?>" role="presentation" />
											</a>
										</li>
									<?php endif; ?>
									<?php $i++; ?>
								<?php endwhile; ?>
							</ul>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; // Password check. ?>

			<div class="post-content">
			<div class="res-page page-result">
				<div class="res-head">
					<?php 
				
					if(isset($_POST["posted_data"])) { 
					$posted_data = json_decode( stripslashes($_POST["posted_data"]), true );
					$posted_data = json_decode( stripslashes($posted_data), true );
					
					
					?>
					<h3><span class="person-name"><?php echo $posted_data["fname"]; ?></span>, based upon this information, please find your best options below:</h3>
					<div class="table-responsive-sm">
						<table class="table">
							<thead>
							  <tr>
								<th scope="col">Age</th>
								<th scope="col">Zip code</th>
								<th scope="col">Gender</th>
								<th scope="col">Tobacco Use</th>
								<th scope="col">Free consult</th>
								</tr>
							</thead>
							<tbody>
							  <tr>
								<td><?php echo $posted_data["age"]; ?></td>
								<td><?php echo $posted_data["zip_code"]; ?></td>
								<td><?php echo $posted_data["gender"]; ?></td>
								<td><?php echo $posted_data["tobacco"]; ?></td>
								<td>Yes</td>
							  </tr>
							</tbody>
						</table>
					</div>
					<?php } ?>
					<p>Call <b>(866) 853-4791</b> to Enroll Now or if you have any questions.</p>
					<p>Our friendly and knowledgeable expert agents are here to assist you.</p>
				</div>
				<div class="res-show page-result-content">
					
					<?php if(isset($_POST["results_data"])) {
						$result_data = json_decode( stripslashes($_POST["results_data"]), true );
						$i=1;
						foreach($result_data as $result) {

					 ?>

					<div class="result-item">
						<div class="result-item-title">
							<h4 data-fontsize="30" data-lineheight="33" style="">
							<?php if($i <= 5) { ?>
							Please Call to Reveal Carrier: <a href="tel:8668534791">(866) 853-4791</a>		
							<?php } else { ?>

								Insurance Carrier: <a href="javascript:void(0);"><?php echo $result["company"]; ?></a>	

						<?php	}	?>												</h4>
						</div>
						<div class="result-item-content">
							<div class="stt">
								<span>No.</span>
								<?php echo $i; ?>							
							</div>
							<div class="images_result">
								<span>Insurance Co.</span>
								
								<p class="f-25">Contact</p><p><img src="https://www.medigap-medicare.com/wp-content/themes/Avada/assets/images/logo-result.png" alt="logo-result"></p><p class="f-22">For Details</p>								
								
							</div>
							<div class="am_best">
								<span>Carrier Rating</span>
								<?php echo $result["am_best"]; ?>							</div>
							<div class="plan">
								<span>Plan Type</span>
								Plan <?php echo $result["plan"]; ?>							</div>
							<div class="monthly">
								<span>Amount</span>
								$<?php echo $result["monthly"]; ?>	 Monthly
							</div>
							<div class="call_now">
								<p>Call Now</p>
								<div class="call">
									<a href="tel:8668534791">(866) 853-4791</a>
								</div>
							</div>
							<div class="error-info">
								<span></span>
								<div class="enroll">
									<a class="btn btn-info btn-lg vc-enroll" data-toggle="modal" data-target="#modal-enroll_3">
										Enroll
<div style="visibility: hidden;">
	<input type="hidden" class="vc-firstname" data-firstname="Test">
	<input type="hidden" class="vc-lastname" data-lastname="Test">
	<input type="hidden" class="vc-gender" data-gender="Male">
	<input type="hidden" class="vc-zipcode" data-zipcode="">
	<input type="hidden" class="vc-age" data-age="65">
	<input type="hidden" class="vc-tobacco" data-tobacco="Tobacco">
	<input type="hidden" class="vc-company" data-company="State Farm">
	<input type="hidden" class="vc-ambest" data-ambest="B+">
	<input type="hidden" class="vc-plan" data-plan="F">
	<input type="hidden" class="vc-monthly" data-monthly="170.07">
	<input type="hidden" class="vc-freeconsult" data-freeconsult="Yes">
</div>
									</a>
									<div class="modal fade" id="modal-enroll_3" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">×</button>
													<p class="modal-title">Thanks Test</p>
												</div>
												<div class="modal-body">
													<p>Thank you for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center" data-fontsize="26" data-lineheight="28" style="">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="more-info">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-more-info_3">More-info</a>
									<div class="modal fade" id="modal-more-info_3" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">×</button>
													<p class="modal-title">Thanks Test</p>
												</div>
												<div class="modal-body">
													<p>Thank you for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center" data-fontsize="26" data-lineheight="28" style="">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>



					<?php
						$i++;

						} 

					}
					?>


				</div>
				</div>
				<?php the_content(); ?>
				<?php fusion_link_pages(); ?>
			</div>
			<?php if ( ! post_password_required( $post->ID ) ) : ?>
				<?php if ( class_exists( 'WooCommerce' ) ) : ?>
					<?php $woo_thanks_page_id = get_option( 'woocommerce_thanks_page_id' ); ?>
					<?php $is_woo_thanks_page = ( ! get_option( 'woocommerce_thanks_page_id' ) ) ? false : is_page( get_option( 'woocommerce_thanks_page_id' ) ); ?>
					<?php if ( Avada()->settings->get( 'comments_pages' ) && ! is_cart() && ! is_checkout() && ! is_account_page() && ! $is_woo_thanks_page ) : ?>
						<?php wp_reset_postdata(); ?>
						<?php comments_template(); ?>
					<?php endif; ?>
				<?php else : ?>
					<?php if ( Avada()->settings->get( 'comments_pages' ) ) : ?>
						<?php wp_reset_postdata(); ?>
						<?php comments_template(); ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; // Password check. ?>
		</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</section>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */