<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        table tr td {
            padding-right: 10px;
        }
    </style>
</head>
<body>
    <p><b>DATE: {!! $date !!}| TIME: {!! $time !!}</b></p>
    <p>IP: {!! $client_ip !!}</p>
    <p>----------------------------</p>
    <p>FIRST NAME: {!! $firstname !!}</p>
    <p>LAST NAME: {!! $lastname !!}</p>
    <p>FREE CONSULT: {!! $freeconsult !!}</p>
    <p>PLAN TYPE: {!! $plan !!}</p>
    <p>AGE: {!! $age !!}</p>
    <p>GENDER: {!! $gender !!}</p>
    <p>TOBACCO: {!! $tobacco !!}</p>
    <p></p>
    <p></p>
    <p></p>
    <table>
        <tr>
            <td>{!! $date !!}</td>
            <td>{!! $time !!}</td>
            <td>{!! $firstname !!}</td>
            <td>{!! $lastname !!}</td>
            <td>{!! $plan !!}</td>
            <td>{!! $age !!}</td>
            <td>{!! $gender !!}</td>
            <td>{!! $tobacco !!}</td>
            <td>{!! $client_ip !!}</td>

            <td>{!! $company !!}</td>
            <td>{!! $ambest !!}</td>
            <td>{!! $monthly !!}</td>
            <td>{!! $platform !!}</td>
            <td>{!! $name_browser !!}</td>
        </tr>
    </table>
</body>
</html>