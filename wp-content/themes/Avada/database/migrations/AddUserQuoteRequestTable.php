<?php 

namespace Database\Migrations;

use Database\Migrations\ExeDB;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
* AddUserQuoteRequestTable
*/
class AddUserQuoteRequestTable extends ExeDB
{
	public $table = 'user_quote';

	public function __construct()
	{
		parent::__construct();
	}

	public function up()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (!Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->create($table_name, function($table){
				$table->increments('id');
				$table->string('firstname', 100);
				$table->string('lastname', 100);
				$table->string('email', 100);
				$table->string('phone', 100);
				$table->integer('result_id');
				$table->timestamps();
			});
		}		
	}

	public function down() {
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->drop($table_name);
		}
	}
}