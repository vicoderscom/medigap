<?php 

namespace Database\Migrations;

use Database\Migrations\ExeDB;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
* 
*/
class AddNaicToLogo extends ExeDB
{
	public $table = 'naic_logo';

	public function __construct()
	{
		parent::__construct();
	}

	public function up()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (!Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->create($table_name, function($table){
				$table->increments('id');
				$table->integer('naic')->unique();
				$table->string('link_logo', 400);
				$table->timestamps();
			});
		}		
	}

	public function down() {
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->drop($table_name);
		}
	}
}