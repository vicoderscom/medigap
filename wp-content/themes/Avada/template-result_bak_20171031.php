<?php

use App\Models\ResultQuote;
use App\Models\UserQuote;
/**
 * Template Name: Result
 * 
 * @package Avada
 * @subpackage Templates
 */

get_header(); 

if(!isset($_GET['id'])) {
	wp_redirect(home_url());
	exit;
}

$userquote = UserQuote::where('id', $_GET['id'])->first();
$resultquote = ResultQuote::find($userquote->result_id);

$resultquote_replace = str_replace('\\', '', $resultquote->result);

$show_result = json_decode($resultquote_replace, true);

$result_plan = [];
$other_plan = [];
if(!empty($show_result['result'])) {
	foreach ($show_result['result'] as $key => $item) {
		if($item['plan'] == $resultquote->plan) {
			$result_plan[] = $item;
		} else {
			$other_plan[] = $item;
		}
	}
}

unset($_COOKIE['plantype']);
unset($_COOKIE['zipcode']);
unset($_COOKIE['age']);
unset($_COOKIE['gender']);
unset($_COOKIE['tobacco']);
unset($_COOKIE['result_quote']);

// $result_quote = [];
// if (isset($_COOKIE['result_quote']) && !empty($_COOKIE['result_quote']) 
//             && ($_COOKIE['plantype'] == $_POST['plantype'] && $_COOKIE['zipcode'] == $_POST['zipcode'] && $_COOKIE['age'] == $_POST['age'] && $_COOKIE['gender'] == $_POST['gender'] && $_COOKIE['tobacco'] == $_POST['tobacco'])) {
//     $result_quote = $_COOKIE['result_quote'];
// 	$result_quote = json_encode($result_quote, true);

// 	unset($_COOKIE['plantype']);
//     unset($_COOKIE['zipcode']);
//     unset($_COOKIE['age']);
//     unset($_COOKIE['gender']);
//     unset($_COOKIE['tobacco']);
//     unset($_COOKIE['result_quote']);
// }
// $plantype = '...';
// $zipcode = '...';
// $age = '...';
// $gender = '...';
// $tobacco = '...';

// if(isset($_COOKIE['plantype']) && !empty($_COOKIE['plantype'])) {
// 	$plantype = $_COOKIE['plantype'];
// }

// if(isset($_COOKIE['zipcode']) && !empty($_COOKIE['zipcode'])) {
// 	$zipcode = $_COOKIE['zipcode'];
// }

// if(isset($_COOKIE['age']) && !empty($_COOKIE['age'])) {
// 	$age = $_COOKIE['age'];
// }

// if(isset($_COOKIE['gender']) && !empty($_COOKIE['gender'])) {
// 	$gender = $_COOKIE['gender'];
// }

$tobacco = '';
if($resultquote['tobacco'] == 'Non-Tobacco') {
	$tobacco = 'not';
} 

?>
<section class="page-result">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-push-2 page-result-content">
				<div class="result-meta">
					<?php 
						// echo '<span>' . __('Thanks ' . $user['firstname'] . '! For a ' . $gender . ' aged ' . $age . ' living in ' . $zipcode . ' who does ' . $tobacco . ' use tobacco, we found these to be the best rates for Plan ' . $plantype . ':', 'medigap-mediacare') . '</span>';
					echo '<span>' . __('Thanks ' . $userquote['firstname'] . '! For a ' . $resultquote['gender'] . ' aged ' . $resultquote['age'] . ' living in ' . $resultquote['zip'] . ' who does ' . $tobacco . ' use tobacco, we found these to be the best rates for Plan ' . $resultquote['plan'] . ':', 'medigap-mediacare') . '</span>';
					?>
				</div>
				<!-- <div class="result-title">
					<h2>Available Plans</h2>
				</div> -->
					<?php 
					$i = 1;
					if(!empty($result_plan)):
						foreach ($result_plan as $key => $item): 								
					?>
					<div class="result-item">
						<div class="result-item-title">
							<h4>American Retirement Life Insurance</h4>
						</div>
						<div class="result-item-content">
							<div><?php echo $i++; ?></div>
							<div><img src="" alt="American Retirement"></div>
							<div><?php echo $item['am_best']; ?></div>
							<div>Plan <?php echo $item['plan']; ?></div>
							<div>$<?php echo $item['monthly']; ?> Monthly</div>
							<div>
								<div class="enroll">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-enroll_<?php echo $i; ?>">Enroll</a>
									<div class="modal fade" id="modal-enroll_<?php echo $i; ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">www.medigap-medicare.com</h4>
												</div>
												<div class="modal-body">
													<p>Thank you for enrolling</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="more-info">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-more-info_<?php echo $i; ?>">More-info</a>
									<div class="modal fade" id="modal-more-info_<?php echo $i; ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">www.medigap-medicare.com</h4>
												</div>
												<div class="modal-body">
													<p>Thank you for enrolling</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endforeach;
					endif;
					//==================== for other part ===================
					$j = 1;
					if(!empty($other_plan)):
						echo '<hr class="hr-class">';
						echo '<h3 class="text-center">Other Plan for you</h3>';
						foreach ($other_plan as $key => $item): 								
					?>
					<div class="result-item other-plan hidden">
						<div class="result-item-title">
							<h4>American Retirement Life Insurance</h4>
						</div>
						<div class="result-item-content">
							<div><?php echo $j++; ?></div>
							<div><img src="" alt="American Retirement"></div>
							<div><?php echo $item['am_best']; ?></div>
							<div>Plan <?php echo $item['plan']; ?></div>
							<div>$<?php echo $item['monthly']; ?> Monthly</div>
							<div>
								<div class="enroll">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-enroll">Enroll</a>
									<div class="modal fade" id="modal-enroll" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">www.medigap-medicare.com</h4>
												</div>
												<div class="modal-body">
													<p>Thanks for</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="more-info">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-more-info">More-info</a>
									<div class="modal fade" id="modal-more-info" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">www.medigap-medicare.com</h4>
												</div>
												<div class="modal-body">
													<p>Thanks for</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endforeach;
					endif;

					if(empty($result_plan) || empty($other_plan)):
					?>
					<div class="result-item">
						<div class="result-item-title">
							<h4>American Retirement Life Insurance</h4>
						</div>
						<span class="text-center">No data</span>
					</div>
					<?php
					endif;
					?>
				<div class="result-button">
					<a class="click_other_part" href="javascript:void(0);" title="">Other parts</a>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-push-2 page-result-content tiep">

</div>

<style type="text/css">
	.block-medigap { display: block; }
	.modal-backdrop.in {
	    z-index: -1;
	}
	.page-result-content .modal {
		top: 150px;
	}
	.page-result-content .modal-dialog {
	    margin-top: 100px;
	}
	.hr-class {
		color: #fff;
		height: 10px;
    	background-color: #fff;
	}
</style>


<?php do_action( 'avada_after_content' ); ?>
<?php get_footer(); ?>

