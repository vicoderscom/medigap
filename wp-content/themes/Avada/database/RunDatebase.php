<?php

namespace Database;

use Database\Migrations\AddNaicToLogo;
use Database\Migrations\AddResutlQuoteTable;
use Database\Migrations\AddUserQuoteRequestTable;
use Database\Seeder\NaicLogoSeeder;

/**
 * Class run migrate database
 */
class RunDatebase
{
    protected $list_tables = [
        AddNaicToLogo::class,
        AddResutlQuoteTable::class,
        AddUserQuoteRequestTable::class
    ];

    public function __construct() {}

    /**
     * [up create table on ddatabase]
     * @return void [description]
     */
    public function up()
    {
        if (current_user_can('administrator')):
            foreach ($this->list_tables as $key => $value) {
                $class = new $value();
                $class->up();
            }
        endif;
    }

    /**
     * [down destroy table on database]
     * @return void [description]
     */
    public function down()
    {
        if (current_user_can('administrator')):
            foreach ($this->list_tables as $key => $value) {
                $class = new $value();
                $class->down();
            }
        endif;
    }

    public function seeder()
    {
        $seeders = [
            NaicLogoSeeder::class,
        ];
        if(!empty($seeders)) {
            foreach ($seeders as $key => $value) {
                $class = new $value();
            }
        }
    }

    /**
     * [addTable name model class of table]
     * @param [type] $table [description]
     */
    public function addTable($table) {
        if (current_user_can('administrator')):
            $class = new $table;
            $class->up();
        endif;
    }

    public function removeTable($table) {
        if (current_user_can('administrator')):
            $class = new $table();
            $class->down();
        endif;
    }
}
