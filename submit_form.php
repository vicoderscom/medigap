<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once( 'twilio/Twilio/autoload.php');

use Twilio\Rest\Client;

$status = false;
$message = "Some error occured.Plz try again";
$data = "";
$posted_data="";

$result_url = "/result-quote-form";
if( isset($_POST['email']) && isset($_POST["fname"]) && isset($_POST["lname"]) && isset($_POST["phone"]) && isset($_POST["zip_code"]) && isset($_POST["age"]) && isset($_POST["gender"]) && isset($_POST["tobacco"]) && isset($_POST["plantype"]) ){
    $fname = $_POST["fname"];
    $lname = $_POST["lname"];
    $email = $_POST['email'];
    $phone = $_POST["phone"];
    $zip_code = $_POST["zip_code"];
    $gender = $_POST["gender"];
    $tobacco = $_POST["tobacco"];
    $plan = $_POST["plantype"];
    $age = $_POST["age"];
    $vc_token_key = $_POST["vc_token_key"];
    $source = $_POST["source"];
    $vc_nonce = $_POST["vc_nonce"];
    $_wp_http_referer = $_POST["_wp_http_referer"];
    $_SESSION['vc_token_key'] = $vc_token_key;
    $result_url .= "?vc_token_key=".$vc_token_key."&vc_nonce=".$vc_nonce."&source=".$source."&_wp_http_referer=".$_wp_http_referer;
    
    $result_url .= "&firstname=".$fname."&lastname=".$lname."&age=".$age."&gender=".$gender."&zipcode=". $zip_code."&phone=".$phone."&tobacco=".$tobacco."&email=".$email."&plantype=".$plan;
    $posted_data = json_encode($_POST);
        if(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL) === false){
            require_once "wp-load.php";


            $token = "b02f06e8ab610bb76b42a2a425b44ec57cdff850";
            $name= $fname." ".$lname;
            $owner_id = "3253989";


           
               $TWILIO_SID = "AC8a09217e65c3e1a4010b567ebedb5660";
               $TWILIO_TOKEN = "4945bad38cc20414d8653698236921f8";


               // $data = $user_data;

                // MailChimp API credentials
                $apiKey = '3a802255ec962551317488c8e6e62803-us18';
                $listID = 'e269c356c8';
                
                // MailChimp API URL
                $memberID = md5(strtolower($email));
                $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
                $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
                
                // member information
                $json = json_encode([
                    'email_address' => $email,
                    'status'        => 'subscribed',
                    'merge_fields'  => [
                        'FNAME'     => $fname,
                        'LNAME'     => $lname
                    ]
                ]);
                
                // send a HTTP POST request with curl
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
                curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                $result = curl_exec($ch);
                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                // store the status message based on response code
                if ($httpCode == 200) {
                    $status = true;

                    $message = '<p style="color: #34A853">Verify your mobile number to get a quote.</p>';
                } else {
                    if( $httpCode) {
                        switch ($httpCode) {
                            case 214:
                                $msg = 'You are already subscribed.';
                                break;
                            default:
                                $msg = 'Some problem occurred, please try again.';
                                break;
                        }
                    $message = '<p style="color: #EA4335">'.$msg.'</p>';

                    }
                }



        } else {

             $message = '<p style="color: #EA4335">Please enter valid email address.</p>';

        }

   
} else {

    $message = '<p style="color: #EA4335">Plz fill all required fields.</p>';
}
        echo json_encode( ["status" => $status, "message" => $message,"data" => $data,"posted_data" => $posted_data,"url" => $result_url]);

