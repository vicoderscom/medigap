jQuery(document).ready(function($) {
    $('#radioBtn a').on('click', function() { //chon gioi tinh
        var val = '';
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#' + tog).prop('value', sel);
        if (sel === 'Y') {
            val = 'Male';
        } else {
            val = 'Female';
        }
        $('#gender').val(val);

        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
    });

    $('#radio-tobaco a').on('click', function() { //chon tp
        var val = '';
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#' + tog).prop('value', sel);

        if (sel === 'Y') {
            val = 'Tobacco';
        } else {
            val = 'Non-Tobacco';
        }
        $('#tobaco').val(val);

        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
    });

    $('.plantype-wrap .plan-out').on('click', function() { //chon abc
        var _this = this;
        $('.plantype-wrap .plan-out').each(function() {
            $(this).removeClass('active');
        });
        $(_this).addClass('active');
        $('.plantype-inp').val($(_this).attr('data-val'));
        if ($('.status').hasClass('active')) {
            $('.status').removeClass('active')
        }
    });

    $('.plantype-wrap .dropdown-inverse > li').on('click', function(e) { //cung chon abc
        var data_val = $(this).attr('val');
        $('.plantype-wrap .plan-out').each(function() {
            $(this).removeClass('active');
        });
        $('.status').text(this.innerHTML);
        $('.status').attr('data-val', data_val);
        $('.status').addClass('active');
        $('.plantype-inp').val(data_val);
    });

    $(".phone_val").mask("(999) 999-9999"); //phone

    $('.show-quote').on('click', function(event) { //button submit
        event.preventDefault();

        var zipcodelength = $('.zipcode_val').val().length;
        console.log(zipcodelength);

        var firstname = $('.firstname_val').val();
        var lastname = $('.lastname_val').val();
        var plantype = $('.plantype-inp').val();
        var zipcode = $('.zipcode_val').val();
        var phone = $('.phone_val').val();
        var age = $('.age_val').val();
        var gender = $('#gender').val();
        var tobaco = $('#tobaco').val();

        if (firstname == "" || lastname == "" || plantype == "" || zipcode == "" || phone == "" || age == "" || gender == "" || tobaco == "") {
            $('.row-title .show-error-medigap').css('display', 'block');
            var titleformE = '<div class="title-formE">Form Not Yet Submitted!</div>';
            var metaformE = '<div class="meta-formE">Sorry, but your form was not submitted! Please correct the following errors and submit the form again:</div>';

            if (firstname == "") {
                var firstnameE = '<span>First Name: This field is required.</span>';
            } else {
                var firstnameE = '';
            }

            if (lastname == "") {
                var lastnameE = '<span>Last Name: This field is required.</span>';
            } else {
                var lastnameE = '';
            }

            if (plantype == "") {
                var plantypeE = '<span>Plantype Name: This field is required.</span>';
            } else {
                var plantypeE = '';
            }

            if (zipcode == "") {
                var zipcodeE = '<span>Zip Code: This field is required.</span>';
            } else {
                var zipcodeE = '';
            }



            if (phone == "") {
                var phoneE = '<span>Phone: This field is required.</span>';
            } else {
                var phoneE = '';
            }

            if (age == "") {
                var ageE = '<span>Age: This field is required.</span>';
            } else {
                var ageE = '';
            }

            if (gender == "") {
                var genderE = '<span>Gender: This field is required.</span>';
            } else {
                var genderE = '';
            }

            if (tobaco == "") {
                var tobacoE = '<span>Tobacco: This field is required.</span>';
            } else {
                var tobacoE = '';
            }
            $('.row-title .show-error-medigap').html(titleformE + metaformE + firstnameE + lastnameE + plantypeE + zipcodeE + phoneE + ageE + genderE + tobacoE);
        }

        if(zipcodelength <= 4 || zipcodelength == 0) {
            console.log("nho hon 5");
            var zipcodeEE = '<span>Zip Code: Please enter 5 numbers.</span>';
            $('.zipcode-inp input').addClass('zipcode-inp-error');

            $('.row-title .show-error-medigap').append(zipcodeEE);

        } else {
            $('.zipcode-inp input').removeClass('zipcode-inp-error');

        }


        var email_value = document.forms["myForm"]["email"].value;
        var pattern = /^.*\@.*\..*$/;
        if (email_value != '' && !pattern.test(email_value)) {
            $('.row-title .show-error-medigap').append('<span class="text-red">Please enter a valid email.</span>');
        } else {

        }


        var check_validate = validate_and_show_error();
        focus_input();

        if(zipcodelength == 5) {
            if (check_validate == true || check_validate === true) {            
                var link = ajax_obj.ajax_url;
                $.ajax({
                    url: ajax_obj.ajax_url,
                    type: "POST",
                    data: {
                        action: 'form_quote_request',
                        firstname: firstname,
                        lastname: lastname,
                        email: email_value,
                        zipcode: zipcode,
                        phone: phone,
                        age: age,
                        tobacco: tobaco,
                        gender: gender,
                        plantype: plantype
                    },
                    beforeSend: function() {
                        $('.spinner-loading.fa-spinner').removeClass('hide');
                        $('.show-quote').addClass('hide');
                    },
                    complete: function() {
                        $('.show-quote').removeClass('hide');
                        $('.spinner-loading.fa-spinner').addClass('hide');
                    },
                    success: function(response) {
                        console.log(response);
                        var res = $.parseJSON(response);
                        if (res.code == 200) {
                            window.location.href = res.link_redirect;
                        } else {
                            $('.row-title .show-error').html('<div class="text-red">' + res.message + '</div>');
                        }
                    },
                    error: function(ex) {
                        console.log(ex + 'error');
                    }
                });
            }
        }
    });

    function isValidEmail(email) {

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(email);

    }

    //===========================================
    function validate_and_show_error() {
        $('.row-title .show-error').text('');
        $('.wrap-form .form-control').css({
            '-webkit-box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)',
            'box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)'
        });
        var sum_flag = 0;

        var firstname = $('.firstname_val').val();
        sum_flag += validate_inp(firstname, '.firstname_val');

        var lastname = $('.lastname_val').val();
        sum_flag += validate_inp(lastname, '.lastname_val');

        var plantype = $('.plantype-inp').val();
        sum_flag += validate_inp(plantype, '.plantype-inp');

        var email = $('.email_val').val();
        // sum_flag += validate_inp(email, '.email_val');
        // if (!isValidEmail(email)) {
        //     $('.email_val').css({
        //         'border-color': 'red',
        //         '-webkit-box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255,0,0,.6)',
        //         'box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255,0,0,.6)',
        //     });
        //     sum_flag += 1;
        // }

        var zipcode = $('.zipcode_val').val();
        sum_flag += validate_inp(zipcode, '.zipcode_val');

        var phone = $('.phone_val').val();
        sum_flag += validate_inp(phone, '.phone_val');

        var age = $('.age_val').val();
        sum_flag += validate_inp(age, '.age_val');

        var gender = $('#gender').val();
        if (gender !== null || gender !== '') {
            $('#radioBtn').css({
                '-webkit-box-shadow': 'none !important',
                'box-shadow': 'none !important'
            });
            $('#radioBtn a').css({
                'border-color': '#39B54A'
            });
        }
        sum_flag += validate_inp(gender, '#radioBtn');

        var tobaco = $('#tobaco').val();
        if (tobaco !== null || tobaco !== '') {
            $('#radio-tobaco').css({
                '-webkit-box-shadow': '0px 0px 0px #fff',
                'box-shadow': '0px 0px 0px #fff'
            });
            $('#radio-tobaco a').css({
                'border-color': '#39B54A'
            });
        }
        sum_flag += validate_inp(tobaco, '#radio-tobaco');

        // console.log(sum_flag);

        if (sum_flag >= 1) {
            // $('.row-title .show-error').html('<div class="text-red"><span>Whooops! Looks Like You Missed Something</span><span>Please Review Form and Submit Again</span></div>');

            if (parseInt(age) < 65) {
                $('.age_val').css({
                    'border-color': 'red',
                    '-webkit-box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255,0,0,.6)',
                    'box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255,0,0,.6)',
                });
                $('.row-title .show-error-medigap').append('<span class="text-red">Your age must from 65 and up *</span>');
                return false;
            }
            return false;
        } else {
            $('.row-title .show-error-medigap').text('');
            if (parseInt(age) < 65) {
                $('.age_val').css({
                    'border-color': 'red',
                    '-webkit-box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255,0,0,.6)',
                    'box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255,0,0,.6)',
                });
                $('.row-title .show-error-medigap').append('<span class="text-red">Your age must from 65 and up *</span>');
                return false;
            }
            return true;
        }
    }

    function validate_inp(inp_val, str_selector) {
        var flag = 0;
        if (inp_val == '' || inp_val === '' || inp_val == null) {
            $(str_selector).css({
                'border-color': 'red',
                '-webkit-box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255,0,0,.6)',
                'box-shadow': 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(255,0,0,.6)',
            });
            $(str_selector + ' a').css({ 'border-color': 'red' })
            flag = 1;
        } else {
            $(str_selector).css({ 'border-color': '#39B54A' });
            flag = 0;
        }
        return flag;
    }

    function focus_input() {
        $('.firstname_val, .lastname_val, .email_val, .phone_val, .age_val, .zipcode_val').on('focus', function() {
            $(this).css({
                'border-color': 'transparent',
                'box-shadow': '0px 0px 0px #fff'
            })
        });
        $('#radioBtn a').on('click', function() {
            $('#radioBtn a').css({
                'border-color': 'transparent',
                'box-shadow': '0px 0px 0px #fff'
            });

            $('#radioBtn a').parent().on('click', function() {
                $(this).css({
                    'border-color': 'transparent',
                    'box-shadow': '0px 0px 0px #fff'
                });
            });
        });
        $('#radio-tobaco a').on('click', function() {
            $('#radio-tobaco a').css({
                'border-color': 'transparent',
                'box-shadow': '0px 0px 0px #fff'
            });

            $('#radio-tobaco a').parent().on('click', function() {
                $(this).css({
                    'border-color': 'transparent',
                    'box-shadow': '0px 0px 0px #fff'
                });
            });
        });
    }
    $(".btn-info").click(function() {
        $(this).parent().find('.modal').modal();
        $(this).parent().find('.modal').addClass("block-medigap");
    });

    $('.click_other_part').click(function() {
        $('.other-plan').removeClass('hidden');
        $(this).attr('disabled')
    });
});