<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style()
{
    // wp_enqueue_style(
    //     'template-style',
    //     asset('app.css'),
    //     false
    // );
}

function theme_enqueue_scripts()
{
    // wp_enqueue_script(
    //     'template-scripts',
    //     asset('app.js'),
    //     'jquery',
    //     '1.0',
    //     true
    // );

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

    $params = array(
       'ajax_url' => admin_url('admin-ajax.php', $protocol)
    );

    wp_localize_script('template-scripts', 'ajax_obj', $params);
}

if (!function_exists('themeSetup')) {
    /**
     * setup support for theme
     *
     * @return void
     */
    function themeSetup()
    {
        // Register menus
        register_nav_menus( array(
            'main-menu' => __('Main Menu', 'vicoders')
        ) );

        $defaults = array(
            'height'      => 40,
            'width'       => 140,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'Tranh đẹp', 'tranhdep' ),
        );
        add_theme_support( 'custom-logo', $defaults );
    }

    add_action('after_setup_theme', 'themeSetup');
}

if (!function_exists('themeSidebars')) {
    /**
     * register sidebar for theme
     *
     * @return void
     */
    function themeSidebars()
    {
        $sidebars = [
            [
                'name'          => __('Sidebar', 'vicoders'),
                'id'            => 'main-sidebar',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer menu 1', 'vicoders'),
                'id'            => 'footer_1',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer menu 2', 'vicoders'),
                'id'            => 'footer_2',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer menu 3', 'vicoders'),
                'id'            => 'footer_3',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer menu 4', 'vicoders'),
                'id'            => 'footer_4',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer menu 5', 'vicoders'),
                'id'            => 'footer_5',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }

    add_action('widgets_init', 'themeSidebars');
}

if(!function_exists('setQueryProduct')) {

    function setQueryProduct( $query ) {
        if (is_admin()) {
            return $query;
        }
        $query->set( 'posts_per_page', 5 );
        // if ( $query->query['post_type'] == 'product' ) {
        //     return;
        // }
        return $query;
    }
    add_action( 'pre_get_posts', 'setQueryProduct', 1 );
}



// login

function custom_login_footer() {

    $args = array(
        'redirect'       => site_url( $_SERVER['REQUEST_URI'] ),
        'form_id'        => 'login-form', //Để dành viết CSS
        'label_username' => __( 'Tên tài khoản' ),
        'id_username'    => 'email',
        'label_password' => __( 'Mật khẩu' ),
        'id_password'    =>'pass',
        'label_remember' => __( 'Ghi nhớ' ),
        'label_log_in'   => __( 'Đăng nhập' ),
    );

    $form = '
        <form name="' . $args['form_id'] . '" id="' . $args['form_id'] . '" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
            ' . $login_form_top . '

            <h3 class="leftstyle">Registered Customers</h3>

            <div class="content">
                <p>If you have an account with us, please log in.</p>
                <ul class="form-list">
                    <li>
                        <label for="' . esc_attr( $args['id_username'] ) . '">' . esc_html( $args['label_username'] ) . '<em>*</em></label>
                        <div class="input-box">
                            <input type="text" name="log" id="' . esc_attr( $args['id_username'] ) . '" class="input" value="' . esc_attr( $args['value_username'] ) . '" size="20" />
                        </div>
                    </li>

                    <li>
                        <label for="' . esc_attr( $args['id_password'] ) . '">' . esc_html( $args['label_password'] ) . '<em>*</em></label>
                        <div class="input-box">
                            <input type="password" name="pwd" id="' . esc_attr( $args['id_password'] ) . '" class="input" value="" size="20" />
                        </div>
                    </li>
                </ul>


                <p class="required" style="margin:0;">* Required Fields</p>
            </div>


            <div class="buttons-set">
                <a href="https://www.bimago.com/customer/account/forgotpassword/" class="f-left">Forgot Your Password?</a>
                <input type="submit" name="wp-submit" id="' . esc_attr( $args['id_submit'] ) . '" class="button button-primary" value="' . esc_attr( $args['label_log_in'] ) . '" />
                <input type="hidden" name="redirect_to" value="' . esc_url( $args['redirect'] ) . '" />
            </div>

        </form>
    ';

    return $form;
}
add_filter('login_form_bottom', 'custom_login_footer');



/* Tự động chuyển đến một trang khác sau khi login */
function my_login_redirect( $redirect_to, $request, $user ) {
        //is there a user to check?
        global $user;
        if ( isset( $user->roles ) && is_array( $user->roles ) ) {
                //check for admins
                if ( in_array( 'administrator', $user->roles ) ) {
                        // redirect them to the default place
                        return admin_url();
                } else {
                        return home_url();
                }
        } else {
                return $redirect_to;
        }
}
 
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

function redirect_login_page() {
    if (is_user_logged_in() ) {
        $login_page  = home_url( '/login/' );

        $page_viewed = basename($_SERVER['REQUEST_URI']);  
     
        if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
            wp_redirect($login_page);
            exit;
        }
    }
}
add_action('init','redirect_login_page');

/* Kiểm tra lỗi đăng nhập */
function login_failed() {
    if (is_user_logged_in()) {
        $login_page  = home_url( '/login/' );
        wp_redirect( $login_page . '?login=failed' );
        exit;
    }
}
add_action( 'wp_login_failed', 'login_failed' );  
 
function verify_username_password( $user, $username, $password ) {
    if (is_user_logged_in()) {
        $login_page  = home_url( '/login/' );
        if( $username == "" || $password == "" ) {
            wp_redirect( $login_page . "?login=empty" );
            exit;
        }
    }
}
add_filter( 'authenticate', 'verify_username_password', 1, 3);


// [bartag foo="foo-value"]
function vicoder_nonce( $atts ) {
    $a = shortcode_atts( array(
        'action_name' => -1,
        'field_name'  => '_wpnonce',
    ), $atts );

    $key = wp_generate_password( 43, false, false );
    $_SESSION['vc_token_key'] = $key;
    
    $html = '<input type="hidden" name="vc_token_key" value="' . $key . '">';
    $html .= '<input type="hidden" name="source" value="' . vicoders_get_source() . '">';
    $html .= wp_nonce_field( $a['action_name'], $a['field_name'], true, false );
    return $html;

}
add_shortcode( 'vicoder_nonce', 'vicoder_nonce' );