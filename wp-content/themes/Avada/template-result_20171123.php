<?php
use App\Models\NaicLogo;
use App\Models\ResultQuote;
use App\Models\UserQuote;
/**
 * Template Name: Result
 * 
 * @package Avada
 * @subpackage Templates
 */

get_header(); 

if(!isset($_SESSION['quote_session'])) {
	wp_redirect(home_url());
	exit;
}
$show_result = $_SESSION['quote_session']['result'];
$info = $_SESSION['quote_session']['info'];


$show_result = json_decode($show_result);
$result_plan = [];
$other_plan = [];
if(!empty($show_result->result)) {
	foreach ($show_result->result as $key => $item) {
		if($item->plan == $info['plan']) {
			$result_plan[] = $item;
		} else {
			$other_plan[] = $item;
		}
	}
}

// echo "<pre>";
// var_dump($other_plan);
// die;

$tobacco = 'tobacco';
if($info['tobacco'] == 'Non-Tobacco') {
	$tobacco = 'non-tobacco';
} 
?>

<!-- Event snippet for Medigap Quote Submission conversion page -->
<script>
gtag('event', 'conversion', {'send_to': 'AW-1067751615/Qg-lCPa-23gQv7GS_QM'});
</script>

<section class="page-result">
	<div class="container result-container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-push-1 page-result-content">
				<div class="result-meta">
					<?php 
					// echo '<span>' . __('Thanks ' . $info['firstname'] . '! For a ' . $info['gender'] . ' aged ' . $info['age'] . ' living in ' . $info['zipcode'] . ' who does ' . $tobacco . ' use tobacco, we found these to be the best rates for Plan ' . $info['plan'] . ':', 'medigap-mediacare') . '</span>';

					echo '<span>'.$info['firstname'].', thank you for your quote request! Based upon the following information ...</span>
					<span>'.$info['gender'].'</span>
					<span>Aged '.$info['age'].'</span>
					<span>Living in '.$info['zipcode'].'</span>
					<span>Uses '.$tobacco.'</span>
					<span>Here are your top options for Plan '.$info['plan'].'. Please contact us so we can provide your the lowest rates.</span>';

					?>
				</div>
					<?php 
					$i = 1;
					if(!empty($result_plan)):
						foreach ($result_plan as $key => $item): 	
							$naic_logo = NaicLogo::where('naic', $item->naic)->first();
							$naic_logo_src = $naic_logo->link_logo;
							if(empty($naic_logo_src)) {
								$naic_logo_src = "http://fakeimg/70x70";
							}
					?>
					<div class="result-item">
						<div class="result-item-title">
							<h4>
								<?php
									if($i == 1 || $i == 2 || $i == 3) {
										echo 'Please Call <a href="tel:8668534791">(866) 853-4791</a> for Information';
									} else {
										echo $item->company . ' - ' . $item->description; 
									}
								?>
							</h4>
						</div>
						<div class="result-item-content">
							<div class="stt">
								<span>No.</span>
								<?php echo $i; ?>
							</div>
							<div class="images_result">
								<span>Company Name</span>
								<?php
									if($i == 1 || $i == 2 || $i == 3) {
										echo '<img src="'.get_template_directory_uri().'/assets/images/logo-result.png" />';
									} else {
										echo '<img src="'.$naic_logo_src.'" alt="'.$item->company.'">';
									}
									$i++;
								?>
							</div>
							<div class="am_best">
								<span>Rating</span>
								<?php echo $item->am_best; ?>
							</div>
							<div class="plan">
								<span>Plan Type</span>
								Plan <?php echo $item->plan; ?>
							</div>
							<div class="monthly">
								<span>Amount</span>
								$<?php echo $item->monthly; ?> Monthly
							</div>
							<div class="call_now">
								<p>Call Now</p>
								<div class="call">
									<a href="tel:8668534791">(866) 853-4791</a>
								</div>
							</div>
							<div class="error-info">
								<span></span>
								<div class="enroll">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-enroll_<?php echo $i; ?>">Call Now<br>(866) 853-4791</a>
									<div class="modal fade" id="modal-enroll_<?php echo $i; ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<p class="modal-title">Thanks <?php echo $info['firstname']; ?></p>
												</div>
												<div class="modal-body">
													<p>Thank you for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="more-info">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-more-info_<?php echo $i; ?>">More-info</a>
									<div class="modal fade" id="modal-more-info_<?php echo $i; ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<p class="modal-title">Thanks <?php echo $info['firstname']; ?></p>
												</div>
												<div class="modal-body">
													<p>Thank you for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
						if($i == 4) {
							echo '<span class="additional-options">Additional Options:</span>';
						}
						endforeach;
					else:
						?>
						<div class="result-item">
							<div class="result-item-title">
								<h4>Result search</h4>
							</div>
							<span class="text-center"><?php echo 'No data for plan ' . $info['plan'] . '. Let see other part!'; ?></span>
						</div>
						<?php
					endif;
					//==================== for other part ===================
					$j = 1;
					if(!empty($other_plan)):
						echo '<hr class="hr-class">';
						echo '<h3 class="text-center">Other Plan for you</h3>';
						foreach ($other_plan as $key => $item): 
							if($j > 30 ){
								break;
							}
							if(empty($item->company)) {
								continue;
							}
							$naic_logo = NaicLogo::where('naic', $item->naic)->first();
							$naic_logo_src = $naic_logo->link_logo;
							if(empty($naic_logo_src)) {
								$naic_logo_src = "http://fakeimg.pl/70x70";
							}							
					?>
					<div class="result-item other-plan hidden">
						<div class="result-item-title">
							<h4><?php echo $item->company . ' - ' . $item->description; ?></h4>
						</div>
						<div class="result-item-content">
							<div><?php echo $j++; ?></div>
							<div><img src="<?php echo $naic_logo_src; ?>" alt="<?php echo $item->company; ?>"></div>
							<div><?php echo $item->am_best; ?></div>
							<div>Plan <?php echo $item->plan; ?></div>
							<div>$<?php echo $item->monthly; ?> Monthly</div>
							<div>
								<div class="enroll">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-enroll-<?php echo $j;  ?>">Enroll</a>
									<div class="modal fade" id="modal-enroll-<?php echo $j; ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<p class="modal-title">Thanks <?php echo $info['firstname']; ?></p>
												</div>
												<div class="modal-body">
													<p>Thank your for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="more-info">
									<a class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal-more-info-<?php echo $j;  ?>">More-info</a>
									<div class="modal fade" id="modal-more-info-<?php echo $j;  ?>" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<p class="modal-title">Thanks <?php echo $info['firstname']; ?></p>
												</div>
												<div class="modal-body">
													<p>Thank your for your inquiry for a medicare supplement quote! A licensed agent will contact you to assist with your needs. If you need immediate assistance, have questions or are ready to signup, please call us at</p>
													<h3 class="text-center">(866) 853-4791</h3>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
						endforeach;
					endif;

					if(empty($result_plan) && empty($other_plan)):
					?>
					<div class="result-item">
						<div class="result-item-title">
							<h4>American Retirement Life Insurance</h4>
						</div>
						<span class="text-center">No data</span>
					</div>
					<?php
					endif;
					?>
				<div class="result-button">
					<a class="click_other_part" href="javascript:void(0);" title="">Other parts</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php do_action( 'avada_after_content' ); ?>
<?php get_footer(); ?>

