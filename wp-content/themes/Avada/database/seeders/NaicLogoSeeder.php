<?php

namespace Database\Seeder;

define('LOGO_NAIC_PATH', THEME_URI . '/resources/assets/company_logos/');

use App\Models\NaicLogo;

class NaicLogoSeeder
{
    public function __construct()
    {
        $this->ExeSeeder();
    }

    public function ExeSeeder()
    {
        $arr_res = [];
        $file    = fopen(LOGO_NAIC_PATH . "_NAIC_to_LOGO_Mapping.csv", "r");
        while (!feof($file)) {
            $arr_res[] = fgetcsv($file);
        }
        fclose($file);
        if (!empty($arr_res)) {
            foreach ($arr_res as $key => $val) {
            	$get_naic = NaicLogo::where('naic', $val[1])->get();
            	if($get_naic->isEmpty() && $val[1] !== null) {
	                $naic_seeder            = new NaicLogo();
	                $naic_seeder->naic      = $val[1];
	                $naic_seeder->link_logo = LOGO_NAIC_PATH . $val[0] . '.jpg';
	                $naic_seeder->save();
            	}
            }
        }
    }
}
