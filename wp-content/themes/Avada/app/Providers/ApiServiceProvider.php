<?php

namespace App\Providers;

use App\RestApi\QuoteApi;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    public $listen = [
        QuoteApi::class
    ];

    public function register()
    {
        foreach ($this->listen as $class) {
            $this->resolveApi($class);
        }
    }

    /**
     * Resolve a restapi instance from the class name.
     *
     * @param  string  $restapi
     * @return restapi instance
     */
    public function resolveApi($restapi)
    {
        return new $restapi();
    }
}
