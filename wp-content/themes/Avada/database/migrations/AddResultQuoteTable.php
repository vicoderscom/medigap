<?php 

namespace Database\Migrations;

use Database\Migrations\ExeDB;
use Illuminate\Database\Capsule\Manager as Capsule;

/**
* 
*/
class AddResutlQuoteTable extends ExeDB
{
	public $table = 'result_quote';

	public function __construct()
	{
		parent::__construct();
	}

	public function up()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (!Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->create($table_name, function($table){
				$table->increments('id');
				$table->string('plan', 100);
				$table->string('gender', 1000);
				$table->string('age', 100);
				$table->string('tobacco', 100);
				$table->string('zip', 100);
				$table->text('result');
				$table->timestamps();
			});
		}		
	}

	public function down() {
		global $wpdb;
		$table_name = $wpdb->prefix . $this->table;
		if (Capsule::Schema()->hasTable($table_name)) {
			Capsule::Schema()->drop($table_name);
		}
	}
}