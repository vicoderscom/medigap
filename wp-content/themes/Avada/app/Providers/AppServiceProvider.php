<?php

namespace App\Providers;

use NF\Facades\App;
use League\Flysystem\Filesystem;
use NF\Database\DatabaseManager;
use League\Flysystem\Adapter\Local;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $listen = [

    ];

    public function register()
    {
        $adapter = new Local($this->app->appPath());
        $this->app->singleton('filesystem', function ($app) use ($adapter) {
            return new Filesystem($adapter);
        });

         $this->app->singleton('DBManager', function ($app) {
            return new DatabaseManager();
        });
    }
}
