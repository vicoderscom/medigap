<?php
/**
 * Widget Class.
 *
 * @author     Vicoders
 * @copyright  (c) Copyright by Vicoders
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}

/**
 * Widget class.
 */
class Fusion_Widget_Form_Quote_Vicoders extends WP_Widget {

	/**
	 * Constructor.
	 *
	 * @access public
	 */
	public function __construct() {

		$widget_ops  = array(
			'classname' => 'form_quote',
			'description' => '',
		);
		$control_ops = array(
			'id_base' => 'form_quote-widget',
		);
		parent::__construct( 'form_quote-widget', 'Vicoders: Form Quote', $widget_ops, $control_ops );

	}

	/**
	 * Echoes the widget content.
	 *
	 * @access public
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance The settings for the particular instance of the widget.
	 */
	public function widget( $args, $instance ) {

		extract( $args );

		$title = apply_filters( 'widget_title', isset( $instance['title'] ) ? $instance['title'] : '' );

		echo $before_widget; // WPCS: XSS ok.

		?>
		<div class="container center-block">
		    <div class="row">
		        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 col-md-push-2 col-lg-push-2 center-block">
		            <div class="wrap-form">
		                <div class="background-form"></div>
		                <div class="text-quote">INSTANT QUOTE</div>
		                <form class="form-customer">
		                    <div class="row row-title">
		                        <div class="title-form"><?php echo $title; ?></div>
		                        <div class="desc-form"><?php echo $instance['desc']; ?></div>
		                    </div>
		                    <div class="wrap-inp">
		                        <div class="row first-last">
		                            <div class="col-xs-12 col-sm-6 col-md-6 firstname-inp">
		                                <label>First Name <span class="text-red">*</span></label>
		                                <input class="form-control" name="firstname" required="required" type="text" />
		                            </div>
		                            <div class="col-xs-12 col-sm-6 col-md-6 lastname-inp">
		                                <label>Last Name <span class="text-red">*</span></label>
		                                <input class="form-control" name="lastname" required="required" type="text" />
		                            </div>
		                        </div>
		                        <div class="row zipcode-phone">
		                            <div class="col-xs-12 col-sm-6 col-md-6 zipcode-inp">
		                                <label>Zip Code <span class="text-red">*</span></label>
		                                <input class="form-control" maxlength="5" name="zipcode" required="required" type="text" />
		                            </div>
		                            <div class="col-xs-12 col-sm-6 col-md-6 phone-inp">
		                                <label>Phone</label>
		                                <input class="form-control" name="phone" required="required" type="tel" />
		                            </div>
		                        </div>
		                        <div class="row plantype-email">
		                            <div class="col-xs-12 col-sm-6 col-md-6">
		                                <label>Plan Type</label>
		                                <div class="nav-container plantype-wrap">
		                                    <div class="form-group">
		                                        <select class="form-control nav plantype-inp" name="plantype">
		                                            <option value="ALL">ALL</option>
		                                            <option value="A">A</option>
		                                            <option value="B">B</option>
		                                            <option value="C">C</option>
		                                            <option value="D">D</option>
		                                            <option value="E">E</option>
		                                            <option value="F">F (High)</option>
		                                            <option value="G">G</option>
		                                            <option value="K">K</option>
		                                            <option value="L">L</option>
		                                            <option value="M">M</option>
		                                        </select>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="col-xs-12 col-sm-6 col-md-6 email-inp">
		                                <label>Email</label>
		                                <input class="form-control" name="email" required="required" type="email" />
		                            </div>
		                        </div>
		                        <div class="row age-gender">
		                            <div class="col-xs-12 col-sm-3 col-md-3 age-inp">
		                                <label>Age <span class="text-red">*</span></label>
		                                <input class="form-control" max="100" min="65" name="age" required="required" type="text" placeholder="From 65 - 100 years old" />
		                            </div>
		                            <div class="col-xs-12 col-sm-3 col-md-3 gender-class">
		                                <div id="radioBtn" class="btn-group"><a class="btn btn-success active" data-toggle="gender-toggle" data-title="Y"><i class="fa fa-male" aria-hidden="true"></i> Male</a><a class="btn btn-success notActive" data-toggle="gender-toggle" data-title="N"><i class="fa fa-female" aria-hidden="true"></i> Female</a></div>
		                                <input id="gender" name="gender" type="hidden" value="Male" />
		                            </div>
		                            <div class="col-xs-12 col-sm-6 col-md-6 tobaco-class">
		                                <div id="radio-tobaco" class="btn-group"><a class="btn btn-success active btn-a-tag" data-toggle="tobaco-toggle" data-title="Y">Tobaco</a><a class="btn btn-success notActive btn-a-tag" data-toggle="tobaco-toggle" data-title="N">Non-Tobaco</a></div>
		                                <input id="tobaco" name="tobaco" type="hidden" value="Tobaco" />
		                            </div>
		                        </div>
		                        <div class="row submit-wrap text-center">
		                            <div class="col-xs-12 col-sm-12 col-md-12">
		                                <button class="btn btn-success btn-lg show-quote" type="submit">Show Me My Quotes</button>
		                            </div>
		                        </div>
		                    </div>
		                </form>
		            </div>
		        </div>
		    </div>
		</div>
		<?php

		echo $after_widget; // WPCS: XSS ok.

	}

	/**
	 * Updates a particular instance of a widget.
	 *
	 * This function should check that `$new_instance` is set correctly. The newly-calculated
	 * value of `$instance` should be returned. If false is returned, the instance won't be
	 * saved/updated.
	 *
	 * @access public
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Settings to save or bool false to cancel saving.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title']    = isset( $new_instance['title'] ) ? $new_instance['title'] : '';
		$instance['desc']    = isset( $new_instance['desc'] ) ? $new_instance['desc'] : '';

		return $instance;

	}

	/**
	 * Outputs the settings update form.
	 *
	 * @access public
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {

		$defaults = array(
			'title'    => 'Title',
			'desc'  => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'Avada' ); ?></label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'desc' ) ); ?>"><?php esc_attr_e( 'Desciption:', 'Avada' ); ?></label>
			<input class="widefat" type="text" id="<?php echo esc_attr( $this->get_field_id( 'desc' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'desc' ) ); ?>" value="<?php echo esc_attr( $instance['desc'] ); ?>" />
		</p>
		<?php

	}
}

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
