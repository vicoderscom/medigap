<?php

if (!function_exists('view')) {
    /**
     * [view description]
     * @param  [type]  $path [description]
     * @param  array   $data
     * @param  boolean $echo [description]
     * @return [type]        [description]
     */
    function view($path, $data = [], $echo = true)
    {
        if ($echo) {
            echo NF\View\Facades\View::render($path, $data);
        } else {
            return NF\View\Facades\View::render($path, $data);
        }
    }
}

if (!function_exists('asset')) {
    /**
     * [asset description]
     * @param [type] $assets [description]
     */
    function asset($assets)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $assets);
    }
}

if (!function_exists('asset2')) {
    /**
     * [asset description]
     * @param [type] $assets [description]
     */
    function asset2($assets2)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/resources/assets/' . $assets2);
    }
}

if (!function_exists('title')) {
    /**
     * 
     * @return string
     */
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name . ' - ' . get_bloginfo('name');
        }

        if (is_404()) {
            return '404 page not found - ' . get_bloginfo('name');
        }

        return get_the_title() . ' - ' . get_bloginfo('name');
    }
}

if (!function_exists('createExcerptFromContent')) {
    /**
     * this function will create an excerpt from post content
     * 
     * @param  string $content
     * @param  int    $limit
     * @param  string $readmore
     * @since  1.0.0
     * @return string $excerpt
     */
    function createExcerptFromContent($content, $limit = 50, $readmore = '...')
    {
        if (!is_string($content)) {
            throw new Exception("first parameter must be a string.");
        }

        if ($content == '') {
            throw new Exception("first parameter is not empty.");
        }

        if (!is_int($limit)) {
            throw new Exception("second parameter must be the number.");
        }

        if ($limit <= 0) {
            throw new Exception("second parameter must greater than 0.");
        }

        $words = explode(' ', $content);

        if (count($words) <= $limit) {
            $excerpt = $words;
        } else {
            $excerpt = array_chunk($words, $limit)[0];
        }
        
        return strip_tags(implode(' ', $excerpt)) . $readmore;
    }
}

if (!function_exists('getPostImage')) {
    /**
     * [getPostImage description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? '' : $img[0];
    }
}

if (!function_exists('price_format')) {
    function price_format($price, $decima = 0)
    {
        return number_format($price, $decima, ',', '.');
    }
}

if(!function_exists('count_nested_categories')) {
    function count_nested_categories($term_id) {
        $child_term = get_term_children($term_id, 'product_cat');
        if(is_wp_error($child_term)) {
            $child_term = [];
        }
        array_push($child_term, $term_id);
        $arg_post_by_idcat = [
            'post_type'      => 'product',
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'tax_query'      => [
                [
                    'taxonomy'         => 'product_cat',
                    'terms'            => $child_term,
                    'field'            => 'term_id',
                    'operator'         => 'IN'
                ],
            ]
        ];
        
        $obj_post = new WP_Query($arg_post_by_idcat);  
        return $obj_post->found_posts;
    }
}

if(!function_exists('sendMailToSalePerson')) {
    function sendMailToSalePerson($to = 'vicoders@gmail.com', $subject = 'Medigap Website - New Lead', $data, $headers)
    {
        if (empty($headers)) {
            $headers = ['Content-Type: text/html; charset=UTF-8'];
        }
        $body = view('emails.EmailForSalePerson', $data, false);
        $sent = wp_mail($to, $subject, $body, $headers);
        return $sent;
    }
}

if(!function_exists('sendMailToSalePersonResult')) {
    function sendMailToSalePersonResult($to = 'vicoders.test@gmail.com', $subject = 'Medigap Website - Result', $data, $headers)
    {
        if (empty($headers)) {
            $headers = ['Content-Type: text/html; charset=UTF-8'];
        }
        $body = view('emails.EmailForResult', $data, false);
        $sent = wp_mail($to, $subject, $body, $headers);
        return $sent;
    }
}

if(!function_exists('getFlatform')) {
    function getFlatform()
    {
        $devices = [
            'iPod',
            'iPhone',
            'iPad',
            'webOS',
            'TouchPad',
            'BlackBerry',
            'BB10',
            'RIM Tablet',
            'SymbianOS',
            'Android',
            'Windows',
            'Touch',
            'Windows Phone',
            'Mac',
        ];
        $checkDevice = 'Undefined';
        foreach ($devices as $key => $device) {
            if(stripos($_SERVER['HTTP_USER_AGENT'], $device)){
                $checkDevice = $device;
            }
        }

        // $u_agent = $_SERVER['HTTP_USER_AGENT']; 
        // $bname = 'Unknown';
        // $platform = 'Unknown';
        // $version= "";
        // if (preg_match('/linux/i', $u_agent)) {
        //     $platform = 'linux';
        // }
        // elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        //     $platform = 'mac';
        // }
        // elseif (preg_match('/windows|win32/i', $u_agent)) {
        //     $platform = 'windows';
        // }
        return $checkDevice;
    }
}

if(!function_exists('getDateDataUser')) {
    function getDateDataUser()
    {
        if(date('H') < 5) {
            $datecheck = date('d')-1;
        } else {
            $datecheck = date('d');
        }

        $datesendmai = date('Y-m-').$datecheck;

        return $datesendmai;
    }
}

if(!function_exists('getTimeDataUser')) {
    function getTimeDataUser()
    {
        if(date('H') < 5) {
            $timecheck = date('H')-'5'+'24';
        } else {
            $timecheck = date('H')-'5';
        }

        $timesendmai = $timecheck.date(':i:s');

        return $timesendmai;
    }
}

if(!function_exists('get_client_ip')) {
    function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }
}

if(!function_exists('get_name_browser')) {
    function get_name_browser()
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/Firefox/i',$user_agent)) $user_agent_browser = 'Firefox'; 
            elseif(preg_match('/Chrome/i',$user_agent)) $user_agent_browser = 'Chrome'; 
            elseif(preg_match('/Opera/i',$user_agent)) $user_agent_browser = 'Opera'; 
            elseif(preg_match('/MSIE/i',$user_agent)) $user_agent_browser = 'IE'; 
            elseif(preg_match('/Safari/i',$user_agent)) $user_agent_browser = 'Safari'; 
            else $user_agent_browser = 'Unknown browser';

        return $user_agent_browser;
    }
}