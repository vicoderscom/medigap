<?php

namespace NF\Database\Eloquent;

use NF\Facades\App;

class Model extends \Illuminate\Database\Eloquent\Model
{
    public function __construct()
    {
        parent::__construct();
        $manager = App::make('DBManager');
        $manager->bootEloquent();
    }
}
