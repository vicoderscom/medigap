jQuery(document).ready(function($) {
    $('#radioBtn a').on('click', function() { //chon gioi tinh
        var val = '';
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#' + tog).prop('value', sel);
        if (sel === 'Y') {
            val = 'Male';
        } else {
            val = 'Female';
        }
        $('#gender').val(val);

        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
    });

    $('#radio-tobaco a').on('click', function() { //chon tp
        var val = '';
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        $('#' + tog).prop('value', sel);

        if (sel === 'Y') {
            val = 'Tobacco';
        } else {
            val = 'Non-Tobacco';
        }
        $('#tobaco').val(val);

        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
    });

    $('.plantype-wrap .plan-out').on('click', function() { //chon abc
        var _this = this;
        $('.plantype-wrap .plan-out').each(function() {
            $(this).removeClass('active');
        });
        $(_this).addClass('active');
        $('.plantype-inp').val($(_this).attr('data-val'));
        if ($('.status').hasClass('active')) {
            $('.status').removeClass('active')
        }
    });

    $('.plantype-wrap .dropdown-inverse > li').on('click', function(e) { //cung chon abc
        var data_val = $(this).attr('val');
        $('.plantype-wrap .plan-out').each(function() {
            $(this).removeClass('active');
        });
        $('.status').text(this.innerHTML);
        $('.status').attr('data-val', data_val);
        $('.status').addClass('active');
        $('.plantype-inp').val(data_val);
    });

    $(".phone_val").mask("(999) 999-9999"); //phone

    var str_error = '';

    $('#quoteForm').validate({

        ignore: [],

        rules: {
            firstname: {
                required: true,
            },

            lastname: {
                required: true,
            },

            zipcode: {
                minlength: 5,
                maxlength: 5,
                required: true,
            },

            email: {
                email: true,
            },

            phone: {
                required: true,
            },

            age: {
                required: true,
                number: true,
                min: 65,
                max: 100,
            },

            gender: {
                required: true
            },

            tobacco: {
                required: true
            }
        },

        messages: {
            firstname: "<span>First Name: This field is required.</span>",
            lastname: "<span>Last Name: This field is required.</span>",
            zipcode: {
                required: "<span>Zip Code: This field is required.</span>",
                minlength: "<span>Zip Code: Please enter 5 numbers.</span>",
                maxlength: "<span>Zip Code: Please enter 5 numbers.</span>"
            },

            phone: "<span>Phone: This field is required.</span>",
            age: {
                required: "<span>Age: This field is required.</span>",
                min: "<span>Your age must from 65 and up.</span>",
                max: "<span>Please enter age between 65 and 100.</span>",
            },

            email: {
                email: "<span>Please enter a valid email.</span>",
            },

            gender: {
                required: "<span>Gender: This field is required.</span>"
            },

            tobacco: {
                required: "<span>Tobacco: This field is required.</span>"
            }
        },
        invalidHandler: function(event, validator) {
            var _this = this;
            _this.str_error = '';
            $('.show-error-medigap').html('');
            validator.errorList.forEach(function(item, index) {
                _this.str_error += '<div>' + item.message + '</div>';
            });

            $('.row-title .show-error-medigap').css('display', 'block');

            var titleformE = '<div class="title-formE">Form Not Yet Submitted!</div>';
            var metaformE = '<div class="meta-formE">Sorry, but your form was not submitted! Please correct the following errors and submit the form again:</div>';

            $('.show-error-medigap').append(titleformE + metaformE + _this.str_error);
        }

    });
    var form = $("#quoteForm");

    $('.show-quote').on('click', function(event) { //button submit
        event.preventDefault();

        var zipcodelength = $('.zipcode_val').val().length;
        var firstname = $('.firstname_val').val();
        var lastname = $('.lastname_val').val();
        var plantype = $('.plantype-inp').val();
        var zipcode = $('.zipcode_val').val();
        var phone = $('.phone_val').val();
        var age = $('.age_val').val();
        var gender = $('#gender').val();
        var tobaco = $('#tobaco').val();
        var email_value = $('input.email_val').val();

        var check_validate = form.valid();
        if (check_validate == true) {
            form.submit();
            // var link = ajax_obj.ajax_url;
            // $.ajax({
            //     url: ajax_obj.ajax_url,
            //     type: "POST",
            //     data: {
            //         action: 'form_quote_request',
            //         firstname: firstname,
            //         lastname: lastname,
            //         email: email_value,
            //         zipcode: zipcode,
            //         phone: phone,
            //         age: age,
            //         tobacco: tobaco,
            //         gender: gender,
            //         plantype: plantype
            //     },
            //     beforeSend: function() {
            //         $('.spinner-loading.fa-spinner').removeClass('hide');
            //         $('.show-quote').addClass('hide');
            //     },
            //     complete: function() {
            //         $('.show-quote').removeClass('hide');
            //         $('.spinner-loading.fa-spinner').addClass('hide');
            //     },
            //     success: function(response) {
            //         console.log(response);
            //         var res = $.parseJSON(response);
            //         if (res.code == 200) {
            //             window.location.href = res.link_redirect;
            //         } else {
            //             $('.row-title .show-error').html('<div class="text-red">' + res.message + '</div>');
            //         }
            //     },
            //     error: function(ex) {
            //         console.log(ex + 'error');
            //     }
            // });

        } else {

            var inputGender = $('#gender').hasClass('error');
            var inputTobaco = $('#tobaco').hasClass('error');

            if (inputGender) {
                $('.btn-success').addClass('error');
            } else {
                $('.btn-success').removeClass('error');
            }

            if ($('#gender').hasClass('valid')) {
                $('#radioBtn .no_gender').removeClass('error');
            }

            if (inputTobaco) {
                $('.btn-a-tag').addClass('error');
            }

            if ($('#tobaco').hasClass('valid')) {
                $('.btn-a-tag').removeClass('error');
            }
        }
    });

    $(".btn-info").click(function() {
        $(this).parent().find('.modal').modal();
        $(this).parent().find('.modal').addClass("block-medigap");
    });

    $('.click_other_part').click(function() {
        $('.other-plan').removeClass('hidden');
        $(this).attr('disabled')
    });

    if ($('button').hasClass('sort_btn')) {
        $('.sort_btn').click(function() {
            var actual_link = $('.actual_link').val();
            var company_sort = $('#company_sort').val();
            var rate_sort = $('#rate_sort').val();
            var price_sort = $('#price_sort').val();
            var params_uri = actual_link;
            var onlyUrl = '';
            params_uri = location.href;

            if (window.location.href.indexOf("company") > -1) {
                params_uri = params_uri.replace(/&?company=([^&]$|[^&]*)/i, "");
            }
            if (company_sort !== undefined && company_sort !== null && company_sort !== '') {
                params_uri += '&company=' + company_sort;
            }

            if (window.location.href.indexOf("am_best") > -1) {
                params_uri = params_uri.replace(/&?am_best=([^&]$|[^&]*)/i, "");
            }
            if (rate_sort !== undefined && rate_sort !== null && rate_sort !== '') {
                params_uri += '&am_best=' + rate_sort;
            }

            if (window.location.href.indexOf("monthly") > -1) {
                params_uri = params_uri.replace(/&?monthly=([^&]$|[^&]*)/i, "");
            }
            if (price_sort !== undefined && price_sort !== null && price_sort !== '') {
                params_uri += '&monthly=' + price_sort;
            }
            window.location.href = params_uri;
        });
    }

    $('#quoteForm').submit(function(event) {
        event.preventDefault();
        var form = this;
        // $('.spinner-loading.fa-spinner').removeClass('hide');
        $('.show-quote').hide();
        $('.popup-sample-report').hide();
        $('.show-quote__waiting').show();
        setTimeout(function() {
            form.submit();
        }, 5000);
    });

    $('.vc-enroll').on('click', function(event) {
        event.preventDefault();
        var vc_firstname = $(this).find('.vc-firstname').data("firstname");
        var vc_lastname = $(this).find('.vc-lastname').data("lastname");
        var vc_gender = $(this).find('.vc-gender').data("gender");
        var vc_zipcode = $(this).find('.vc-zipcode').data("zipcode");
        var vc_age = $(this).find('.vc-age').data("age");
        var vc_tobacco = $(this).find('.vc-tobacco').data("tobacco");
        var vc_company = $(this).find('.vc-company').data("company");
        var vc_ambest = $(this).find('.vc-ambest').data("ambest");
        var vc_plan = $(this).find('.vc-plan').data("plan");
        var vc_monthly = $(this).find('.vc-monthly').data("monthly");
        var vc_freeconsult = $(this).find('.vc-freeconsult').data("freeconsult");

        // console.log(ajax_obj.ajax_url);
        // console.log(vc_firstname);
        // console.log(vc_lastname);
        // console.log(vc_gender);
        // console.log(vc_zipcode);
        // console.log(vc_age);
        // console.log(vc_tobacco);
        // console.log(vc_company);
        // console.log(vc_ambest);
        // console.log(vc_plan);
        // console.log(vc_monthly);
        // console.log(vc_freeconsult);

        jQuery.ajax({
            type: "POST",
            url: ajax_obj.ajax_url,
            data: {
                action: 'vc_send_mail_result',
                vc_send_mail_firstname: vc_firstname,
                vc_send_mail_lastname: vc_lastname,
                vc_send_mail_gender: vc_gender,
                vc_send_mail_zipcode: vc_zipcode,
                vc_send_mail_age: vc_age,
                vc_send_mail_tobacco: vc_tobacco,

                vc_send_mail_company: vc_company,
                vc_send_mail_ambest: vc_ambest,
                vc_send_mail_plan: vc_plan,
                vc_send_mail_monthly: vc_monthly,
                vc_send_mail_freeconsult: vc_freeconsult,
            },
            success: function(response) {
                // data = jQuery.parseJSON(response);
                // console.log(data);
                console.log(response);
            }
        });

    });

    
        

});