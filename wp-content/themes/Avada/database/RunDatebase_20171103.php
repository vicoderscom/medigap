<?php

namespace Database;

use Database\Migrations\AddResutlQuoteTable;
use Database\Migrations\AddUserQuoteRequestTable;

/**
 * Class run migrate database
 */
class RunDatebase
{
    protected $list_tables = [
        AddResutlQuoteTable::class,
        AddUserQuoteRequestTable::class
    ];

    public function __construct() {}

    /**
     * [up create table on ddatabase]
     * @return void [description]
     */
    public function up()
    {
        if (current_user_can('administrator')):
            foreach ($this->list_tables as $key => $value) {
                $class = new $value();
                $class->up();
            }
        endif;
    }

    /**
     * [down destroy table on database]
     * @return void [description]
     */
    public function down()
    {
        if (current_user_can('administrator')):
            foreach ($this->list_tables as $key => $value) {
                $class = new $value();
                $class->down();
            }
        endif;
    }

    public function seeder()
    {
        $seeders = [
            // UserExtendSeeder::class,
        ];
        if(!empty($seeders)) {
            foreach ($seeders as $key => $value) {
                $class = new $value();
            }
        }
    }
}
