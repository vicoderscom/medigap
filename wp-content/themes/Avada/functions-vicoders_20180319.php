<?php
ob_clean();
ob_start();
session_start();

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

use App\Models\ResultQuote;
use App\Models\UserQuote;
use Database\RunDatebase;
use GuzzleHttp\Client;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

define('THEME_PATH', Avada::$template_dir_path);
define('THEME_URI', get_template_directory_uri());

require __DIR__ . '/vendor/autoload.php';

$app = require_once Avada::$template_dir_path . '/bootstrap/app.php';

/**
 *
 */
class VicodersSetting
{
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'addStyleScript']);
    }

    public function addStyleScript()
    {
        wp_enqueue_style('fusion-font-awesome', FUSION_LIBRARY_URL . '/assets/fonts/fontawesome/font-awesome.css', [], '1.0.0');
        wp_enqueue_style('bootstrap-3.3.7', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', [], '1.0.0');
        wp_enqueue_style('avada-IE-fontawesome', FUSION_LIBRARY_URL . '/assets/fonts/fontawesome/font-awesome.css', [], '1.0.0');
        wp_enqueue_style('form-quote-style', Avada::$template_dir_url . '/assets/css/form-quote.css', [], '1.0.2');
        wp_style_add_data('avada-IE-fontawesome', 'conditional', 'lte IE 9');

        wp_enqueue_script('bootstrap-quote', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
        wp_enqueue_script('masked-input', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js');
        wp_enqueue_script('form-quote', Avada::$template_dir_url . '/assets/js/form-quote.js', [], '1.0.2');
        wp_enqueue_script('jquery.validate', Avada::$template_dir_url . '/bower_components/jquery-validation/dist/jquery.validate.js');

        $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

        $params = [
            'ajax_url' => admin_url('admin-ajax.php', $protocol),
        ];

        wp_localize_script('form-quote', 'ajax_obj', $params);
    }
}

new VicodersSetting();

if (isset($_GET['run'])) {
    $rundb = new RunDatebase();
    switch ($_GET['run']) {
        case 'up':
            $rundb->up();
            break;
        case 'down':
            $rundb->down();
            break;
        case 'seeder':
            $rundb->seeder();
            break;
        default:
            break;
    }
}
if (isset($_GET['addtable'])) {
    $rundb = new RunDatebase();
    $rundb->addTable($_GET['addtable']);
}

if (isset($_GET['removetable'])) {
    $rundb = new RunDatebase();
    $rundb->removeTable($_GET['removetable']);
}

if ( ! function_exists('vicoders_get_source') ) {
    function vicoders_get_source()
    {
        // source = [google, bing, yahoo, other, unknown].
        $source = '';

        // Get previous link.
        if (!$_SESSION['referrer']) {
            $str_referer = $_SERVER['HTTP_REFERER'];
            $_SESSION['referrer'] = $str_referer;
        }
        $get_referer = $_SESSION['referrer'];

        // Check source.
        if ( null === $get_referer ) {
            $source = 'unknown';
        } else {
            $source = 'other';
        }

        $know = ['google', 'bing', 'yahoo'];
        foreach ($know as $item) {
            if (strpos($get_referer, $item) !== false){
                $source = $item;
            }
        }
        return $source;
    }

}

//ajax
add_action('wp_ajax_vc_send_mail_result', 'Vc_send_mail_result');
add_action('wp_ajax_nopriv_vc_send_mail_result', 'Vc_send_mail_result');
function Vc_send_mail_result() {


    $user_data_result = [
        'firstname' => $_POST["vc_send_mail_firstname"],
        'lastname' => $_POST["vc_send_mail_lastname"],
        'age'       => $_POST["vc_send_mail_age"],
        "gender"    => $_POST["vc_send_mail_gender"],
        "plan"      => $_POST["vc_send_mail_plan"],
        "tobacco"   => $_POST["vc_send_mail_tobacco"],
        "zipcode"   => $_POST["vc_send_mail_zipcode"],

        "company"   => $_POST["vc_send_mail_company"],
        "ambest"    => $_POST["vc_send_mail_ambest"],
        "monthly"   => $_POST["vc_send_mail_monthly"],

        "date"      => getDateDataUser(),
        "time"      => getTimeDataUser(),
        "client_ip" => get_client_ip(),
        "platform"  => getFlatform(),
    ];

    echo "send mail ajax success";

    $emails[] = get_option('admin_email');
    if ( defined('VC_EMAILS') && ! empty(json_decode(VC_EMAILS, true)) ) {
        $emails = json_decode(VC_EMAILS, true);
    }
    foreach ($emails as $key => $value) {
        // echo $value;
        sendMailToSalePersonResult($to = $value, $subject = 'Medigap Website - Result', $user_data_result, $headers);
    }
    

}
