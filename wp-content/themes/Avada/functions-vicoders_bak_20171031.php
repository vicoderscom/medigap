<?php
ob_clean();
ob_start();

use App\Models\ResultQuote;
use App\Models\UserQuote;
use Database\RunDatebase;
use GuzzleHttp\Client;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require __DIR__ . '/vendor/autoload.php';

$app = require_once Avada::$template_dir_path . '/bootstrap/app.php';

/**
 *
 */
class VicodersSetting
{
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'addStyleScript']);

        add_action('wp_ajax_form_quote_request', [$this, 'FormQuoteRequest']);
        add_action('wp_ajax_nopriv_form_quote_request', [$this, 'FormQuoteRequest']);
    }

    public function addStyleScript()
    {
        wp_enqueue_style('fusion-font-awesome', FUSION_LIBRARY_URL . '/assets/fonts/fontawesome/font-awesome.css', [], '1.0.0');
        wp_enqueue_style('bootstrap-3.3.7', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', [], '1.0.0');
        wp_enqueue_style('avada-IE-fontawesome', FUSION_LIBRARY_URL . '/assets/fonts/fontawesome/font-awesome.css', [], '1.0.0');
        wp_enqueue_style('form-quote-style', Avada::$template_dir_url . '/assets/css/form-quote.css', [], '1.0.2');
        wp_style_add_data('avada-IE-fontawesome', 'conditional', 'lte IE 9');

        wp_enqueue_script('bootstrap-quote', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
        wp_enqueue_script('masked-input', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js');
        wp_enqueue_script('form-quote', Avada::$template_dir_url . '/assets/js/form-quote.js', [], '1.0.2');

        $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

        $params = [
            'ajax_url' => admin_url('admin-ajax.php', $protocol),
        ];

        wp_localize_script('form-quote', 'ajax_obj', $params);
    }

    /*
     * Send mail to admin
     * return True or False
     * */
    public function sendMail($email, $data)
    {
        $mail = new PHPMailer();                              // Passing `true` enables exceptions
        //Server settings
        $mail->SMTPDebug = 2;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'vicoders.test@gmail.com';                 // SMTP username
        $mail->Password = 'bkapspqwndcxwixp';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('vicoders.test@gmail.com', 'Vicoders');
        $mail->addAddress('phungbaochung@gmail.com', 'Chung');     // Add a recipient

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Here is the subject';
        $mail->Body = "Hi " . $data['firstname'] . " " . $data['lastname'] . " wellcome to mysite, your phone is " . $data['phone'] . ", your email is " . $data['email'] . ", your age is " . $data['age'] . ", your gender is " . $data['gender'] . ", your plan is " . $data['plan'] . "your tobacco is " . $data['tobacco'] . ", your zipcode is " . $data['zipcode'];
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
        return true;

    }

    public function sendMailWP($to = 'vicoders@gmail.com', $subject = 'Medigap-Medicare', $data, $headers) {
        $body = "Hi " . $data['firstname'] . " " . $data['lastname'] . " wellcome to mysite, your phone is " . $data['phone'] . ", your email is " . $data['email'] . ", your age is " . $data['age'] . ", your gender is " . $data['gender'] . ", your plan is " . $data['plan'] . "your tobacco is " . $data['tobacco'] . ", your zipcode is " . $data['zipcode'];
        if(empty($headers)) {
            $headers = array('Content-Type: text/html; charset=UTF-8');
        }
         
        $sent = wp_mail( $to, $subject, $body, $headers );
        return $sent;
    }

    public function FormQuoteRequest()
    {

//        $mail = $this->sendMail();
//        var_dump($mail);die();
        $save_result = '';
        if (empty($_POST)) {
            $data = [
                'code' => 204,
                'message' => 'Data not sent !',
            ];
            return json_encode($data);
        }

        // if (isset($_COOKIE['result_quote'])
        //     && !empty($_COOKIE['result_quote'])
        //     && $_COOKIE['plantype'] == $_POST['plantype']
        //     && $_COOKIE['zipcode'] == $_POST['zipcode']
        //     && $_COOKIE['age'] == $_POST['age']
        //     && $_COOKIE['gender'] == $_POST['gender']
        //     && $_COOKIE['tobacco'] == $_POST['tobacco']
        // ) {
        //     $save_result = $_COOKIE['result_quote'];
        // }

        $get_resultquote = ResultQuote::where('plan', $_POST['plantype'])->where('gender', $_POST['gender'])->where('age', $_POST['age'])->where('tobacco', $_POST['tobacco'])->where('zip', $_POST['zipcode'])->first();
        if (empty($save_result) || $save_result == null) {
            if (!$get_resultquote) {
                $client = new Client();
                $res = $client->request('GET', site_url('/index.php/wp-json/wp/v2/quote-api'));
                $data_json = $res->getBody();
                $data = json_decode($data_json, true);
                $save_result = $data_json;
            } else {
                $save_result = $get_resultquote->result;
            }
        }

        if (empty($save_result) || $save_result === null) {
            $res = [
                'code' => '204',
                'message' => 'Not found data'
            ];
            echo json_encode($res);
            die;
        }

        if (!$get_resultquote) {
            $result_quote = new ResultQuote;
            $result_quote->zip = $_POST['zipcode'];
            $result_quote->plan = $_POST['plantype'];
            $result_quote->age = $_POST['age'];
            $result_quote->gender = $_POST['gender'];
            $result_quote->tobacco = $_POST['tobacco'];
            $result_quote->result = $save_result;
            $result_quote->save();
            $id_save = $result_quote->id;
        } else {
            $get_resultquote->result = $save_result;
            $get_resultquote->save();
            $id_save = $get_resultquote->id;
        }

        $user_quote = new UserQuote();
        $user_quote->firstname = $_POST['firstname'];
        $user_quote->lastname = $_POST['lastname'];
        $user_quote->phone = $_POST['phone'];
        $user_quote->email = $_POST['email'];
        $user_quote->result_id = $id_save;
        $user_quote->save();
        $data = [
            'firstname' => $_POST['firstname'],
            'lastname' => $_POST['lastname'],
            'phone' => $_POST['phone'],
            'email' => $_POST['email'],
            'age' => $_POST['age'],
            "gender" => $_POST['gender'],
            "plan" => $_POST['plantype'],
            "tobacco" => $_POST['tobacco'],
            "zipcode" => $_POST['zipcode']
        ];

        // setcookie("plantype", $_POST['plantype'], time() + (3600 * 14));
        // setcookie("zipcode", $_POST['zipcode'], time() + (3600 * 14));
        // setcookie("age", $_POST['age'], time() + (3600 * 14));
        // setcookie("gender", $_POST['gender'], time() + (3600 * 14));
        // setcookie("tobacco", $_POST['tobacco'], time() + (3600 * 14));
        // setcookie("result_quote", $save_result, time() + (3600 * 14));

        $checkSent = $this->sendMailWP($_POST['email'], 'Medigap-Medicare', $data, '');
        $checkSent = $this->sendMailWP('daudq.info@gmail.com', 'Medigap-Medicare', $data, '');

        $res = [
            'code' => '200',
            'message' => 'success',
            'link_redirect' => site_url('result-quote-form/?id=' . $user_quote->id)
        ];
        echo json_encode($res);
        die;
        // fastcgi_finish_request();
        // $this->sendMail('phungbaochung@gmail.com', $data);
    }
}

new VicodersSetting();

if (isset($_GET['run'])) {
    $rundb = new RunDatebase();
    switch ($_GET['run']) {
        case 'up':
            $rundb->up();
            break;
        case 'down':
            $rundb->down();
            break;
        case 'seeder':
            $rundb->seeder();
            break;
        default:
            break;
    }
}
