<?php

namespace App\Models;

use NF\Database\Eloquent\Model;

/**
 *
 */
class UserQuote extends Model
{
    /**
     * [$table_name name of table]
     * @var string
     */
    protected $table = PREFIX_TABLE . 'user_quote';

    /**
     * [$primary_id primary key of table]
     * @var string
     */
    protected $primary_key = 'id';

    const STATUS_TRUE  = 1;
    const STATUS_FALSE = 0;

    protected $fillable = ['firstname', 'lastname', 'phone', 'email', 'result_id', 'created_at', 'updated_at'];

}
