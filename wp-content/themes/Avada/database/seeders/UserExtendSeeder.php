<?php 

namespace Database\Seeder;

use App\Models\UserExtend;
/**
* 
*/
class UserExtendSeeder
{
	public function __construct()
	{
		$this->ExeSeeder();
	}

	public function ExeSeeder() {
		$args = array(
	        'orderby'      => 'login',
	        'order'        => 'ASC',
	        'fields'       => 'ID'
	     ); 
	    $all_user = get_users( $args ); 
	    foreach ($all_user as $key => $val_id) {
	    	$user_ex = new UserExtend();
	    	$user_ex->user_id = $val_id;
	    	$user_ex->save();
	    }
	}
}